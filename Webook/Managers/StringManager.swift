//
//  StringManager.swift
//  Webook
//
//  Created by BALA SEKHAR on 19/01/21.
//

import Foundation
let baseUrl:String = "http://b2cdomain.in/serenity/api/"

let userIdUserDefaults: String = "NSUserDefaultsKeyOfUserId"
let userMobileNumberUserDefaults : String = "NSUserDefaultsKeyOfUserMobileNumber"

let registrationFlowStatusUserDefaults: String = "NSUserDefaultsKeyOfAppFlowStatus"
let userLoginStatusUserDefaults: String = "NSUserDefaultsKeyOfUserLoginStatus"
let userProfileDetailsUserDefaults: String = "NSUserDefaultsKeyOfUserProfileDetails"
let firebaseAuthVerificationID: String = "NSUserDefaultsFirebaseAuthVerificationID"


let registeredSignUpFlowValue:String = "Registered"
let phoneNumberVerifiedSignUpFlowValue:String = "PhoneNumberVerified"
let profileUpdatedSignUpFlowValue:String = "ProfileUpdated"
let paymentDoneSignUpFlowValue:String = "PaymentDone"
let completedSignUpFlowValue:String = "Completed"

struct keys {
    static let token = "Token"
}

class StringManager {
    
    static let sharedInstance = StringManager()
    
    private init() {
    }
    
    
    
    func validateEmail(text: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._ %+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: text)
    }
    
    
    
    func validateMobileLength(text: String) -> Bool {
        if text.count >= 3 {
            return true
        } else {
            return false
        }
    }
    
    func validateMobile(value: String) -> Bool {
        let PHONE_REGEX = "^[6-9]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result = phoneTest.evaluate(with: value)
        return result
    }
    
    
    func validatePassword(text: String) -> Bool {
        if text.count >= 6 {
            return true
        } else {
            return false
        }
    }
    
    
    func validateName(text: String) -> Bool {
        if text.count > 0 {
            return true
        } else {
            return false
        }
    }
}
