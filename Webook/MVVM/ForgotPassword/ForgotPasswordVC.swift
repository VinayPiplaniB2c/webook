//
//  ForgotPasswordVC.swift
//  Webook
//
//  Created by BALA SEKHAR on 15/01/21.
//

import UIKit
import SkyFloatingLabelTextField
class ForgotPasswordVC: HKBaseViewController {

  
    @IBOutlet weak var mobileTextField: SkyFloatingLabelTextFieldWithIcon!
    
    var loginViewModel: LoginViewModel!
    var verifyViewModel:VerifyOTPViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        loginViewModel = LoginViewModel(viewController: self)
        
        loginViewModel.fotgotPasswordDidGetData = {[weak self] in
            print("ForgotPasswordResponse from login")
            self?.loginViewModel?.viewController?.isFromSignUp = false
            AppDelegate.shared?.isFromSignUp = false
            self?.goToVerifyFromForgotPassword()
        }
       

        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        setStatusBar(backgroundColor: UIColor(red: 255/255, green:243/255, blue: 0/255, alpha: 1.0))

    }
   
    private func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }


    @IBAction func continueAction(_ sender: UIButton) {
        if mobileTextField.text == ""{
            showAlert(withTitle: "Message", andMessage: "Please enter the email address")
        }else if !StringManager.sharedInstance.validateEmail(text: mobileTextField.text!) {
            showAlert(withTitle: "Message", andMessage: "Please enter valid email address in format:name@example.com")
        }else{
            loginViewModel.forgotPassword(email: mobileTextField.text ?? "")
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if segue.identifier == "segueVerifyForgotPassword" {
             if let verifyOtpViewController = segue.destination as? VerifyOTPViewController{
                 verifyOtpViewController.sourceScreenName = "forgetPasswordVC"
             }
         }
     }
    
}
