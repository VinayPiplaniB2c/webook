//
//  CityService.swift
//  Webook
//
//  Created by Apple on 27/01/21.
//

import Foundation

class CityService {
    var requestBody = WBRequest()
    func city(success: @escaping Success, failure: @escaping Failure) {
        SharedClient.sharedInstance.start(request: CityRequest.city(cityRequestPayload: requestBody as! CityRequestPlayload), model:CityResponse(), success: { data in
            success(data)
        }, failure: { error in
            failure(error)
        })
    }
}
