//
//  SelectCityViewController.swift
//  Webook
//
//  Created by BALA SEKHAR on 16/01/21.
//

import UIKit
import Kingfisher
class SelectCityViewController: HKBaseViewController {
    
    
    @IBOutlet weak var cityCollectionView: UICollectionView!
    
    var cityViewModel: CityModelView?
    var cityResponseModel: CityResponse?
    var cityData:[CityList]=[]
    
    var selectedIndex:Int=0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cityCollectionView.delegate=self
        cityCollectionView.dataSource=self
        
        cityViewModel = CityModelView(viewController: self)
        cityViewModel?.CityAPI()
        setupViewModelCallBacks()
    }
    override func viewDidAppear(_ animated: Bool) {
        setStatusBar(backgroundColor: UIColor(red: 255/255, green:243/255, blue: 0/255, alpha: 1.0))
        
    }
    
    private func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }
    
    
    
    func setupViewModelCallBacks() {
        
        cityViewModel?.serverErrorStatus = { [weak self] error in
            self?.showAlert(withTitle: "Message", andMessage: error.localizedDescription)
        }
        
        cityViewModel?.didGetData = {[weak self] model in
            print("CityViewModel : -->\(model)")
            self?.citySetupUI(cityResponseModel: model)
            
        }
    }
    func citySetupUI(cityResponseModel: CityResponse?) {
        if let citysResponseModel = cityResponseModel {
            self.cityResponseModel = citysResponseModel
            self.cityCollectionView.reloadData()
        }
    }
    //SelectAreaViewController-segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToSelectArea" {
            let areaVC:SelectAreaViewController = segue.destination as! SelectAreaViewController
            areaVC.itemId=cityResponseModel?.cityList?[selectedIndex]
        }
    }
}
extension SelectCityViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cityResponseModel?.cityList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionCell:CityCollectionViewCell=cityCollectionView.dequeueReusableCell(withReuseIdentifier: "CityCollectionViewCellId", for: indexPath) as! CityCollectionViewCell
        
        let model = cityResponseModel?.cityList?[indexPath.row]
        collectionCell.cityName.text = model?.name
        let imageUrlStr = model?.image
        let escapedString = imageUrlStr?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        //        print("SelectCity Images Str is \(escapedString ?? "")")
        let imageURL = URL(string: escapedString ?? "")
        DispatchQueue.main.async {
            collectionCell.cityImage.kf.setImage(with: imageURL)
        }
        
        return collectionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  40
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        performSegue(withIdentifier: "segueToSelectArea", sender: self)
        
    }
    
}

