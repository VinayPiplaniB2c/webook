//
//  CityModelView.swift
//  Webook
//
//  Created by Apple on 27/01/21.
//

import Foundation
class CityModelView: HKBaseViewModel {
    var internetConnectionStatus: (() -> ())?
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var serverErrorStatus: ((HKErrorCase) -> ())?
    var didGetData: ((CityResponse) -> ())?
    var validationFailure: ((String) -> ())?
    var validationSuccess: (() -> ())?
    
    private weak var cityViewController:  SelectCityViewController?

    //MARK: -- Network checking
    /// Define networkStatus for check network connection
    var networkStatus = Reach().connectionStatus()
    
    /// Define boolean for internet status, call when network disconnected
    var isDisconnected: Bool = false {
        didSet {
            self.alertMessage = "No network connection. Please connect to the internet"
            self.internetConnectionStatus?()
        }
    }
    
    //MARK: -- UI Status
    
    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    override init(viewController: HKBaseViewController) {
        
        super.init(viewController: viewController)
        cityViewController = viewController as? SelectCityViewController
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
    }
    
    //MARK: Internet monitor status
    @objc func networkStatusChanged(_ notification: Notification) {
        self.networkStatus = Reach().connectionStatus()
    }
    //MARK: -- CityAPI
    func CityAPI() {
        switch networkStatus {
        case .offline:
            self.isDisconnected = true
            self.internetConnectionStatus?()
        case .online:
            cityApi()
        default:
            break
        }
    }
}
extension CityModelView {
    
    private func cityApi() {
        self.cityViewController?.showLoadingView()
       
        let cityRequest = CityRequestPlayload()
        let cityService = CityService()
        cityService.requestBody = cityRequest
        
      
        cityService.city(success: { [weak self] response in
            self?.cityViewController?.hideLoadingView()
            if let responseModel = response as? CityResponse {
                print("cityresponseModel:--->\(responseModel)")
                self?.didGetData?(responseModel)
            }
        }) { [weak self] (error) in
            self?.cityViewController?.hideLoadingView()
            if let error = error as? HKErrorCase{
                self?.handleGenericErrors(error: error)
            }
            print(error.debugDescription)
        }
    }
}
