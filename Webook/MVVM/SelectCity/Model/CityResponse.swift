//
//  CityResponse.swift
//  Webook
//
//  Created by Apple on 27/01/21.
//

import Foundation
import ObjectMapper

struct CityResponse :  Mappable{
   
    var status: Int?
    var msg: String?
    var cityList : [CityList]?
   
    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        status <- map["status"]
        msg <- map["message"]
        cityList <- map["city_list"]
        
    }
}

struct CityList : Mappable{
    
    var id : Int?
    var name : String?
    var image : String?
   

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        image <- map["image"]
        
    }
}
