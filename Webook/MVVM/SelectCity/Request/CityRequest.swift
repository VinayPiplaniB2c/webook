//
//  CityRequest.swift
//  Webook
//
//  Created by Apple on 27/01/21.
//

import Foundation
import  Alamofire
enum CityRequest: Request {
    case city(cityRequestPayload:CityRequestPlayload)
    var path: String {
        return APIEndPoints.city.endpoint
    }
    var method: HTTPMethod {
        switch self {
        case .city:
            return .post
        }
    }
    var parameters: [String: Any]? {
        return [:]
    }
    var headers: [String: String]? {
        switch self {
        case .city:
            let dictionary = ["Accept": "application/json"]
            return dictionary
        }
    }
    var cachePolicy: URLRequest.CachePolicy {
        return .reloadIgnoringCacheData
    }
    
    var dataType: DataType {
        return DataType.JSON
    }
}
