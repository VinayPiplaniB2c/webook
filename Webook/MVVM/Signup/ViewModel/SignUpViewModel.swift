//
//  SignUpViewModel.swift
//  Webook
//
//  Created by BALA SEKHAR on 13/01/21.
//

import Foundation

class SignUpViewModel:HKBaseViewModel {
    
    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: (() -> ())?
    var didGetData: (() -> ())?
    var validationFailure: ((String) -> ())?
    var validationSuccess: (() -> ())?
    
    var authToken:String = ""
    private weak var signupViewController: SignupViewController?
    
    //MARK: -- Network checking
    /// Define networkStatus for check network connection
    var networkStatus = Reach().connectionStatus()
    
    /// Define boolean for internet status, call when network disconnected
    var isDisconnected: Bool = false {
        didSet {
            self.alertMessage = "No network connection. Please connect to the internet"
            self.internetConnectionStatus?()
        }
    }
    
    //MARK: -- UI Status
    
    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    override init(viewController: HKBaseViewController) {

        super.init(viewController: viewController)
        signupViewController = viewController as? SignupViewController

        NotificationCenter.default.addObserver(self, selector: #selector(self.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()

    }
    
    //MARK: Internet monitor status
    @objc func networkStatusChanged(_ notification: Notification) {
        self.networkStatus = Reach().connectionStatus()
    }
    
    //MARK: -- SignUpAPI
    func signUpAPI(name: String?, email: String?, phone: String?, password: String?) {
        switch networkStatus {
        case .offline:
            self.isDisconnected = true
            self.internetConnectionStatus?()
        case .online:
            signupAPI(name:name, email: email, phone: phone, password: password)
        default:
            break
        }
    }
    
    //MARK: -- Verify Phone With FireBase Auth
//    func verifyPhone(mobileNumber: String?) {
//        switch networkStatus {
//        case .offline:
//            self.isDisconnected = true
//            self.internetConnectionStatus?()
//        case .online:
//            verifyPhoneWithFireBaseAuth(mobileNumber: mobileNumber)
//        default:
//            break
//        }
//    }
}

extension SignUpViewModel {

    private func signupAPI(name: String?, email: String?, phone: String?, password: String?) {
        
        self.signupViewController?.showLoadingView()
        
        self.getBaseRequestModel(name: name ?? "", email: email ?? "", phone: phone ?? "", password: password ?? "")
        
        let signupRequest = SignUpRequestPayload(deviceType: "ios",
                                                 deviceId: "12345",
                                                 phone: phone ?? "",
                                                 password: password ?? "",
                                                 name: name ?? "",
                                                 email: email ?? "",
                                                 referralCode: "")
        
       
        let signupService = SignUpService()
        signupService.requestBody = signupRequest
        signupService.signup(success: { [weak self] response in
            print("response: \(response)")
            self?.signupViewController?.hideLoadingView()
            if let responseModel = response as? SignupResponseModel {
                print("rsponse : \(responseModel)")
                
                if responseModel.status == 0 && responseModel.data == nil {
                    self?.signupViewController?.showAlert(withTitle: "Error", andMessage: responseModel.msg ?? "Some error occure. Please try again.")
                    return
                }
                
                UserDefaults.standard.set(responseModel.data?.id, forKey: "userId")
                UserDefaults.standard.set(phone, forKey: "mobileNo")

            }
                            
            self?.validationSuccess?()
            
        }) { [weak self] (error) in
            self?.signupViewController?.hideLoadingView()
            if let error = error as? HKErrorCase {
                self?.handleGenericErrors(error: error)
            }
        }
    }

//     private func verifyPhoneWithFireBaseAuth(mobileNumber: String?) {
//        self.didGetData?()
//    }
    
    func getBaseRequestModel(name: String?, email: String?, phone: String?, password: String?) {
            
            if let phone = phone {
                if let password = password {
                    if let name = name {
                        if let email = email {
                            
                            if AppDelegate.shared?.baseRequestModel == nil {
                                AppDelegate.shared?.baseRequestModel = HKBaseRequestModel(name : name, email : email, phone: phone , password : password)
                            }
                        }
                    }
                }
            }
        }
}
