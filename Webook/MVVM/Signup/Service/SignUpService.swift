
//  SignUpService.swift
//  Webook
//
//  Created by BALA SEKHAR on 21/01/21.
//

import Foundation

class SignUpService {
    var requestBody = WBRequest()
    
    func signup(success: @escaping Success, failure: @escaping Failure) {
        SharedClient.sharedInstance.start(request: SignUpRequests.signup(signUpRequestPayload: requestBody as! SignUpRequestPayload), model: SignupResponseModel(), success: { data in
            success(data)
        }, failure: { error in
            failure(error)
        })
    }
    func signupStep2(success: @escaping Success, failure: @escaping Failure) {
        SharedClient.sharedInstance.start(request: VerifyOtpRequests.verifyotp(verifyOTPRequestPayload: requestBody as! VerifyOtpRequestPayload), model: VerifyOTPResponseModel(), success: { data in
            success(data)
        }, failure: { error in
            failure(error)
        })
    }
  
    
   
}
