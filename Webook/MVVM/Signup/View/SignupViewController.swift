//
//  SignupViewController.swift
//  Webook
//
//  Created by BALA SEKHAR on 13/01/21.
//

import UIKit
import SkyFloatingLabelTextField

class SignupViewController: HKBaseViewController {
    
    // MARK:- IBOutlet
    
    @IBOutlet weak var usernameTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var phonenumberTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var refrralCodeTextField: SkyFloatingLabelTextFieldWithIcon!
    
    @IBOutlet weak var checkmarkBoxOutlet: UIButton!
    
    //Popupview Connections
    @IBOutlet var termsAndConditionPopupView: UIView!
    @IBOutlet weak var roundCheckMarkOutlet: UIButton!
    
    private var viewControlVisualEffectView  = UIVisualEffectView()
    
    var signupViewModel: SignUpViewModel!
    var acceptTerms:Bool = false
    var popupAcceptTerms:Bool = false
    
    let eyeButtonOne = UIButton(type: .custom)
    let eyeButtonTwo = UIButton(type: .custom)
    
    var eyeButtonOneBool:Bool = false
    var eyeButtonTwoBool:Bool = false
    // MARK:- ViewcontrollerLifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signupViewModel = SignUpViewModel(viewController: self)
        eyeButtonSetup()
        setupViewModelCallBacks()
    }
    func setupViewModelCallBacks() {

        signupViewModel.validationSuccess = {[weak self] in
//            self?.signupViewModel?.verifyPhone(mobileNumber: AppDelegate.shared?.baseRequestModel?.phone ?? "")
            self?.goToOTPScreen()

        }
        

    }
    func eyeButtonSetup(){
        //eyeone
        eyeButtonOne.setImage(UIImage(named: "Closeeye_Icon"), for: .normal)
        eyeButtonOne.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        eyeButtonOne.frame = CGRect(x: CGFloat(passwordTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        eyeButtonOne.addTarget(self, action: #selector(eyeOneButton), for: .touchUpInside)
        passwordTextField.rightView = eyeButtonOne
        passwordTextField.rightViewMode = .always
        
        //eyetwo
        eyeButtonTwo.setImage(UIImage(named: "Closeeye_Icon"), for: .normal)
        eyeButtonTwo.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        eyeButtonTwo.frame = CGRect(x: CGFloat(confirmPasswordTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        eyeButtonTwo.addTarget(self, action: #selector(eyeTwoButton), for: .touchUpInside)
        confirmPasswordTextField.rightView = eyeButtonTwo
        confirmPasswordTextField.rightViewMode = .always
    }
    @objc func eyeOneButton(_ sender: Any) {
        
        if eyeButtonOneBool {
            eyeButtonOneBool = false
            eyeButtonOne.setImage(UIImage(named:"Closeeye_Icon"), for: UIControl.State.normal)
            passwordTextField.isSecureTextEntry=true
        } else {
            eyeButtonOneBool = true
            eyeButtonOne.setImage(UIImage(named:"Openeye_Icon"), for: UIControl.State.normal)
            passwordTextField.isSecureTextEntry=false
        }
        
    }
    @objc func eyeTwoButton(_ sender: Any) {
        if eyeButtonTwoBool {
            eyeButtonTwoBool = false
            eyeButtonTwo.setImage(UIImage(named:"Closeeye_Icon"), for: UIControl.State.normal)
            confirmPasswordTextField.isSecureTextEntry=true
        } else {
            eyeButtonTwoBool = true
            eyeButtonTwo.setImage(UIImage(named:"Openeye_Icon"), for: UIControl.State.normal)
            confirmPasswordTextField.isSecureTextEntry=false
            
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        setStatusBar(backgroundColor: UIColor(red: 255/255, green:243/255, blue: 0/255, alpha: 1.0))
        
    }
    
    private func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }
    // MARK:- IBActions

    @IBAction func signinAction(_ sender: UIButton) {
//        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = storyBoard.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
//        vc.modalPresentationStyle = .fullScreen
//        present(vc, animated: true)
        
    }
    
    
    @IBAction func checkMarkBoxAction(_ sender: UIButton) {
        selectTerms()
    }
    
    @IBAction func termsAction(_ sender: UIButton) {
        popviewSetup()
        
    }
    
    @IBAction func privacyAction(_ sender: UIButton) {
        
    }
    
    @IBAction func roundCheckMarkButton(_ sender: UIButton) {
        selectPopupViewTerms()
    }
    
    @IBAction func acceptAction(_ sender: UIButton) {
        viewControlVisualEffectView.isHidden = true
        termsAndConditionPopupView.isHidden = true
        
    }
    
    @IBAction func getStartedAction(_ sender: UIButton) {
        if usernameTextField.text == "" || emailTextField.text == "" || phonenumberTextField.text == "" || passwordTextField.text == "" || confirmPasswordTextField.text == ""{
            showAlert(withTitle: "Message", andMessage: "Fields can not be empty")

        }else if !StringManager.sharedInstance.validateName(text: usernameTextField.text!){
            showAlert(withTitle: "Message", andMessage: "Please enter your name")
        }else if !StringManager.sharedInstance.validateEmail(text: emailTextField.text!) {
            showAlert(withTitle: "Message", andMessage: "Please enter valid email address in format:name@example.com")
        }else if !StringManager.sharedInstance.validateMobile(value: phonenumberTextField.text!) {
            showAlert(withTitle: "Message", andMessage: "Please enter valid mobile number")
        }else if !StringManager.sharedInstance.validatePassword(text: passwordTextField.text!) {
            showAlert(withTitle: "Message", andMessage: "Please use minimum 6 or more characters to create password")
        }else if !StringManager.sharedInstance.validatePassword(text: confirmPasswordTextField.text!) {
            showAlert(withTitle: "Message", andMessage: "Please use minimum 6 or more characters to create password")
        }else if !acceptTerms || !popupAcceptTerms {
                showAlert(withTitle: "Message", andMessage: "Please accept terms and conditions.")
        }else{
            
            let name = usernameTextField.text ?? ""
            let email = emailTextField.text ?? ""
            let phone = phonenumberTextField.text ?? ""
            let password = passwordTextField.text ?? ""
            
            if let confirmPassword = confirmPasswordTextField.text {
                if password != confirmPassword {
                    showAlert(withTitle: "Message", andMessage: "Confirm password should be match with password")
                    return
                }
            }
            
            signupViewModel.signUpAPI(name: name, email: email, phone: phone, password: password)
        }
        

    }
    private func selectPopupViewTerms() {
        if popupAcceptTerms {
            popupAcceptTerms = false
            roundCheckMarkOutlet.setImage(UIImage(named:"RoundbuttonOff_Icon"), for: UIControl.State.normal)
        } else {
            popupAcceptTerms = true
            roundCheckMarkOutlet.setImage(UIImage(named:"Roundbuttonon_Icon"), for: UIControl.State.normal)
        }
    }
    private func selectTerms() {
        if acceptTerms {
            acceptTerms = false
            checkmarkBoxOutlet.setImage(UIImage(named:"Checkbox_Icon"), for: UIControl.State.normal)
        } else {
            acceptTerms = true
            checkmarkBoxOutlet.setImage(UIImage(named:"Checkmark_Icon"), for: UIControl.State.normal)
        }
    }
    private func popviewSetup(){
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        viewControlVisualEffectView.isHidden = false
        viewControlVisualEffectView.effect=UIBlurEffect(style: .light)
        self.view.addSubview(viewControlVisualEffectView)
        viewControlVisualEffectView.translatesAutoresizingMaskIntoConstraints = false
        viewControlVisualEffectView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        viewControlVisualEffectView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        viewControlVisualEffectView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        viewControlVisualEffectView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        
        termsAndConditionPopupView.isHidden = false
        termsAndConditionPopupView.center = self.view.center
        self.view.addSubview(termsAndConditionPopupView)
        
        
    }
    
    
}
