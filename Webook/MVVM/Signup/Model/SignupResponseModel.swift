
//  SignupResponseModel.swift
//  Webook
//
//  Created by BALA SEKHAR on 21/01/21.
//
import Foundation
import ObjectMapper

struct SignupResponseModel :  Mappable {

    var status: Int?
    var msg: String?
    var isOtpVerified: Int?
    var data : SignupData?


    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        data <- map["data"]
        msg <- map["message"]
        status <- map["status"]
        isOtpVerified <- map["isOtpVerified"]
    }
}

struct SignupData : Mappable {

    var id : Int?

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        id <- map["id"]
    }
}
