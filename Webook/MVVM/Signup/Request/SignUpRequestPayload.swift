
//  SignUpRequestPayload.swift
//  Webook
//
//  Created by BALA SEKHAR on 21/01/21.
//
import Foundation
import ObjectMapper

class SignUpRequestPayload: WBRequest {
    
    var deviceType: String?
    var deviceId: String?
    
    var phone: String?
    var password: String?
    var name: String?
    var email: String?
    var referralCode: String?
    
    init(deviceType: String, deviceId: String, phone: String, password: String, name: String, email: String, referralCode: String) {
        
        super.init()
        parameters?["device_type"] = deviceType
        parameters?["device_id"] = deviceId
        parameters?["phone"] = phone
        parameters?["password"] = password
        parameters?["name"] = name
        parameters?["email"] = email
        parameters?["referal_code"] = referralCode
    }
    
    required init?(map: Map) {
        super.init(map: map)
        
        deviceType <- map["device_type"]
        deviceId <- map["device_id"]
        phone <- map["phone"]
        password <- map["password"]
        name <- map["name"]
        email <- map["email"]
        referralCode <- map["referal_code"]
    }
}
