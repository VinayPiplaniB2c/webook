
//  SignUpRequests.swift
//  Webook
//
//  Created by BALA SEKHAR on 21/01/21.
//

import Foundation
import Alamofire

enum SignUpRequests: Request {
    case signup(signUpRequestPayload: SignUpRequestPayload)
    
    var path: String {
        switch self {
        case .signup:
            let path = APIEndPoints.signup.endpoint
            return path
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .signup :
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case let .signup(signUpRequestPayload):
            return signUpRequestPayload.parameters
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .signup :
            let dictionary = ["Accept": "application/json"]
            return dictionary
        }
    }
    
    var cachePolicy: URLRequest.CachePolicy {
        return .reloadIgnoringCacheData
    }
    
    var dataType: DataType {
        return DataType.JSON
    }
}
