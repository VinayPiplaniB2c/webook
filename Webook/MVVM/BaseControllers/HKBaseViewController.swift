//
//  HKParentViewController.swift
//  HumanityKuotient
//
//  Created by Satya on 11/09/20.
//

import UIKit

class HKBaseViewController: UIViewController {
    
    static var window: UIWindow?
    var isFromSignUp: Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib
    }
    
  
    
    func showAlert(withTitle title: String, andMessage message: String, completion: ((UIAlertAction) -> Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: completion)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithCancel(withTitle title: String, andMessage message: String, completion: ((UIAlertAction) -> Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: .cancel, handler: completion)
        let noAction = UIAlertAction(title: "No", style: .default, handler: completion)
        alert.addAction(noAction)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLoadingView(){
        LoadingIndicator.shared.showLoadingIndicator()
    }
    
    func hideLoadingView(){
        LoadingIndicator.shared.hideLoadingIndicator()
    }
    
//    func skipTutorialScreen() {
//        self.performSegue(withIdentifier: "skipLoginSegue", sender: nil)
//    }
    
    func goToOTPScreen() {
        self.performSegue(withIdentifier: "verifyOtpSegue", sender: nil)
    }
    
    
    func goToOTPScreenFromLogin() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "verifyOtpSegueFromLogin", sender: nil)
        }
    }
    
    func goToSelectCityScreen() {
        self.performSegue(withIdentifier: "segueSelectCity", sender: nil)
    }
    
    func goToVerifyFromForgotPassword() {
        self.performSegue(withIdentifier: "segueVerifyForgotPassword", sender: nil)
    }
    
    func goToChangePasswordFromForgot() {
        self.performSegue(withIdentifier: "segueForgetWithChangePassword", sender: nil)
    }
    func goToSelectArea() {
        self.performSegue(withIdentifier: "segueToSelectArea", sender: nil)
    }
    func goToPopularLibraries() {
        self.performSegue(withIdentifier: "goToPopularLibraries", sender: nil)
    }

    func goToBrandsList() {
        self.performSegue(withIdentifier: "goToBrandsList", sender: nil)
    }
        
    func goToHomeTabBarController(){
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        if let tabBarViewController = storyboard.instantiateViewController(identifier: "DashboardTabbarController") as? DashboardTabbarController {
            if let window = UIApplication.shared.windows.first {
                window.rootViewController = tabBarViewController
                window.makeKeyAndVisible()
            }
        }
    }
//    func goToSelectCityScreen(){
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        if let tabBarViewController = storyboard.instantiateViewController(identifier: "SelectCityViewController") as? SelectCityViewController {
//            if let window = UIApplication.shared.windows.first {
//                window.rootViewController = tabBarViewController
//                window.makeKeyAndVisible()
//            }
//        }
//    }
    
//    func goToHomeWithTab(index: Int){
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        if let tabBarViewController = storyboard.instantiateViewController(identifier: "HomeTabBarController") as? HomeTabBarController {
//            tabBarViewController.selectedIndex = index
//        }
//    }
    
//    func goToLogin(){
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        if let loginNavigationController = storyboard.instantiateViewController(identifier: "LoginNavigationController") as? UINavigationController {
//            if let window = UIApplication.shared.windows.first {
//                window.rootViewController = loginNavigationController
//                window.makeKeyAndVisible()
//            }
//        }
//    }
    
    func goToTremsAndConditions(){

    }
    
    func goToWorkNoteScreen(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "segueWorkNote", sender: nil)
    }
    
    func goToStartRespondScreen(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "startRespondSeque", sender: nil)
    }
    
    func goToFamilyRelationshipScreen(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "sequeFamilyRelationship", sender: nil)
    }
    
    func goToFamilyRelationshipWithAssessmentScreen(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "sequeFamilyRelationshipWithAssessment", sender: nil)
    }
    
    func goToTakeAssessmentFromRespondScreen(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "sequeTakeAssessmentFromRespondScreen", sender: nil)
    }
    
    func goToDowloadOverAllReportScreen(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "dowloadOverAllReportSeque", sender: nil)
    }
    
    func goToDirectDowloadReportScreen(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "directDowloadReportSeque", sender: nil)
    }
    
    func handleUnauthorizedError(){
        showAlert(withTitle:"Message", andMessage: "Unknown error. Please try after some time.", completion: { action in
//            self.goToLogin()
        })
    }
    
    func goToOverAllReportWithMinRequest(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "segueOverAllReport", sender: nil)
    }
    
    func goToOverAllReportWithMinFeedback(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "segueOverAllReportWithMinFeedback", sender: nil)
    }
    
    func goToDetailReport(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "sequeDetailReport", sender: nil)
    }
    
    func goToDetailReportWithMinFeedback(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "segueDetailedReportWithMinFeedback", sender: nil)
    }
    
    func goToDowloadDetailReportScreen(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "dowloadDetailReportSeque", sender: nil)
    }
    
    func goToDowloadAggreeDetailReportScreen(viewController: UIViewController?) {
        self.performSegue(withIdentifier: "dowloadAggreeDetailReportSeque", sender: nil)
    }
    
    func setToolBarWithCancelButton(enable: Bool, tag: Int) -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePressOnPicker(sender:)))
        doneButton.tag = tag
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.cancelPressOnPicker(sender:)))

        if enable {
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        } else {
            toolBar.setItems([spaceButton, doneButton], animated: false)
        }
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }

    @objc func donePressOnPicker(sender: UIBarButtonItem) {

    }

    @objc func cancelPressOnPicker(sender: UIBarButtonItem) {

    }
}
