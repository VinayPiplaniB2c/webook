//
//  HKBaseRequestModel.swift
//  HumanityKuotient
//
//  Created by Satya on 11/10/20.
//

import Foundation
import ObjectMapper

struct HKBaseRequestModel : Codable {

    let name : String?
    let email : String?
    let phone : String?
    let password : String?
    
    init(name : String?, email : String?, phone : String?, password : String?){
        self.email = email
        self.name = name
        self.phone = phone
        self.password = password
    }
}
