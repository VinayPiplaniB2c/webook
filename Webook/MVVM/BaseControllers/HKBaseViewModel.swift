//
//  HKParentViewModel.swift
//  HumanityKuotient
//
//  Created by Satya on 11/09/20.
//

import Foundation

class HKBaseViewModel {
    
    weak var viewController: HKBaseViewController?
    
    init(){ }
    
    init(viewController: HKBaseViewController) {
        self.viewController = viewController
    }
    
    func initWith(viewController: HKBaseViewController) {
        self.viewController = viewController
    }
    
    func handleGenericErrors(error: HKErrorCase){
        switch error {
        case .unauthorized:
            self.viewController?.handleUnauthorizedError()
        case let .unprocessableEntity(_, code, model):
            self.viewController?.showAlert(withTitle: "\(code ?? 0)", andMessage: "\((model as? HKError)?.errorMessage ?? "")")
        case .noInternetConnection:
            self.viewController?.showAlert(withTitle: "Message", andMessage: "No internet connection")
        default:
            self.viewController?.showAlert(withTitle: "Message", andMessage: "\(error.localizedDescription). Please try again")
        }
    }
}
