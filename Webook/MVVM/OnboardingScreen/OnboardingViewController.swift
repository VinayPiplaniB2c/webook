//
//  OnboardingViewController.swift
//  Webook
//
//  Created by BALA SEKHAR on 12/01/21.
//

import UIKit
struct Page {
    let onboardImage: UIImage
    let description: String
}

class OnboardingViewController: HKBaseViewController {
    
    // MARK:- IBActions
    @IBOutlet weak var onboardCollectionView: UICollectionView!
    @IBOutlet weak var onboardPagecontrol: UIPageControl!
    
    var currentIndex: Int = 0
    let pages: [Page] = [Page(onboardImage: #imageLiteral(resourceName: "OboardingImageOne"), description: "Search the libraries near by you"),
                         Page(onboardImage: #imageLiteral(resourceName: "OboardingImageTwo"), description: "Book your library seat for prefered location "),
                         Page(onboardImage: #imageLiteral(resourceName: "OboardingImageThree"), description: "Enjoy studying with the study material")
    ]
    // MARK:- ViewcontrollerLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onboardingCollectionViewSetup()
        customPageControll()
    }
    // MARK:- CustomMethods
    
    private func customPageControll(){
        onboardPagecontrol.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 2, y: 2)
        }
        onboardPagecontrol.numberOfPages = pages.count
    }
    private func onboardingCollectionViewSetup(){
        
        onboardCollectionView.register(UINib(nibName: "OnboardingCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "OnboardingCollectionViewCellId")
        onboardCollectionView.delegate=self
        onboardCollectionView.dataSource=self

        
    }
    
    // MARK:- IBActions
    @IBAction func skipAction(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
        
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        currentIndex += 1
        
        if currentIndex >= pages.count {
            
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true)
        }
        
        let indexPath = IndexPath(row: currentIndex, section: 0)
        onboardCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
    }
    
    @IBAction func pageChange(_ sender: Any) {
        let pc = sender as! UIPageControl
        
        // scrolling the collectionView to the selected page
        onboardCollectionView.scrollToItem(at: IndexPath(item: pc.currentPage, section: 0),
                                           at: .centeredHorizontally, animated: true)
    }
    // To update the UIPageControl
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        onboardPagecontrol.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        
    }
    
}
// MARK:- Extensions

extension OnboardingViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    // MARK:- collectionView dataSource & collectionView FlowLayout delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell:OnboardingCollectionViewCell=onboardCollectionView.dequeueReusableCell(withReuseIdentifier: "OnboardingCollectionViewCellId", for: indexPath) as? OnboardingCollectionViewCell else{
            fatalError("onboardingCollectionViewCellId not found")
        }
        
        
        cell.configureCell(page: pages[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:onboardCollectionView.frame.width, height: onboardCollectionView.frame.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout
                            collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    
}
