//
//  OnboardingCollectionViewCell.swift
//  Webook
//
//  Created by BALA SEKHAR on 12/01/21.
//

import UIKit

class OnboardingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var onboardImage: UIImageView!
    @IBOutlet weak var onboardLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    func configureCell(page: Page){
        
        // set the title and description of the screen
        self.onboardImage.image = page.onboardImage
        self.onboardLabel.text = page.description

    }

}
