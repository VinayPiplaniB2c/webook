//
//  AreaResponse.swift
//  Webook
//
//  Created by Apple on 28/01/21.
//

import Foundation
import ObjectMapper

struct AreaResponse :  Mappable{
   
    var status: Int?
    var msg: String?
    var areaList : [AreaList]?
   
    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        status <- map["status"]
        msg <- map["message"]
        areaList <- map["area_list"]
        
    }
}

struct AreaList : Mappable{
    
    var id : Int?
    var name : String?
   

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        
    }
}
