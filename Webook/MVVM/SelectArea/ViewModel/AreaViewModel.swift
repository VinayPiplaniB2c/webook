//
//  AreaViewModel.swift
//  Webook
//
//  Created by Apple on 28/01/21.
//

import Foundation
class AreaViewModel: HKBaseViewModel {
    var internetConnectionStatus: (() -> ())?
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var serverErrorStatus: ((HKErrorCase) -> ())?
    var areaDidGetData: ((AreaResponse) -> ())?
    var validationFailure: ((String) -> ())?
    var validationSuccess: (() -> ())?
    
    private weak var areaViewController:  SelectAreaViewController?

    //MARK: -- Network checking
    /// Define networkStatus for check network connection
    var networkStatus = Reach().connectionStatus()
    
    /// Define boolean for internet status, call when network disconnected
    var isDisconnected: Bool = false {
        didSet {
            self.alertMessage = "No network connection. Please connect to the internet"
            self.internetConnectionStatus?()
        }
    }
    
    //MARK: -- UI Status
    
    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    override init(viewController: HKBaseViewController) {
        
        super.init(viewController: viewController)
        areaViewController = viewController as? SelectAreaViewController
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
    }
    
    //MARK: Internet monitor status
    @objc func networkStatusChanged(_ notification: Notification) {
        self.networkStatus = Reach().connectionStatus()
    }
    //MARK: -- CityAPI
    func AreaAPI(cityid:String?) {
        switch networkStatus {
        case .offline:
            self.isDisconnected = true
            self.internetConnectionStatus?()
        case .online:
            areaApi(cityid: cityid)
        default:
            break
        }
    }
}
extension AreaViewModel {
    
    private func areaApi(cityid:String?) {
        self.areaViewController?.showLoadingView()
       
        let areaRequest = AreaRequestPlayload(cityid: cityid ?? "")
        let areaService = AreaService()
        areaService.requestBody = areaRequest
        
      
        areaService.area(success: { [weak self] response in
            self?.areaViewController?.hideLoadingView()
            if let responseModel = response as? AreaResponse {
                print("arearesponseModel:--->\(responseModel)")
                self?.areaDidGetData?(responseModel)
            }
        }) { [weak self] (error) in
            self?.areaViewController?.hideLoadingView()
            if let error = error as? HKErrorCase{
                self?.handleGenericErrors(error: error)
            }
            print(error.debugDescription)
        }
    }
}
