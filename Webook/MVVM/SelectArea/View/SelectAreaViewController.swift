//
//  SelectAreaViewController.swift
//  Webook
//
//  Created by BALA SEKHAR on 16/01/21.
//

import UIKit

class SelectAreaViewController: HKBaseViewController {

    @IBOutlet weak var areaListTableview: UITableView!
   
    var areViewModel: AreaViewModel?
    var areaResponseModel: AreaResponse?
    
    var itemId : CityList?

    override func viewDidLoad() {
        super.viewDidLoad()
        areViewModel = AreaViewModel(viewController: self)
        
        areViewModel?.AreaAPI(cityid:"\(itemId?.id ?? 143)")
        setupViewModelCallBacks()
        setupTabelView()
    }
    override func viewDidAppear(_ animated: Bool) {
        setStatusBar(backgroundColor: UIColor(red: 255/255, green:243/255, blue: 0/255, alpha: 1.0))
        
    }
    
    private func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }
    private func setupViewModelCallBacks() {
        
        areViewModel?.serverErrorStatus = { [weak self] error in
            self?.showAlert(withTitle: "Message", andMessage: error.localizedDescription)
        }
        
        areViewModel?.areaDidGetData = {[weak self] model in
            print("AreaViewModel : -->\(model)")
            self?.areaResponseModel = model
            self?.areaListTableview.reloadData()
        }
    }
    private func setupTabelView(){
        areaListTableview.delegate=self
        areaListTableview.dataSource=self
    }
}
extension SelectAreaViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return areaResponseModel?.areaList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let areaCell:SelectAreaTableViewCell=areaListTableview.dequeueReusableCell(withIdentifier: "SelectAreaTableViewCellId", for: indexPath) as! SelectAreaTableViewCell
        let model = areaResponseModel?.areaList?[indexPath.row]
        areaCell.areaNameLabel.text = model?.name
        return areaCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.goToHomeTabBarController()

    }
    
    
}
