//
//  SelectAreaTableViewCell.swift
//  Webook
//
//  Created by Apple on 04/02/21.
//

import UIKit

class SelectAreaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var areaNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(red: 255/255, green:250/255, blue: 152/255, alpha: 1.0)
        selectedBackgroundView = backgroundView
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        
        // Configure the view for the selected state
    }
    
}
