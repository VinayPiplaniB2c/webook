//
//  AreaService.swift
//  Webook
//
//  Created by Apple on 28/01/21.
//

import Foundation

class AreaService {
    var requestBody = WBRequest()
    func area(success: @escaping Success, failure: @escaping Failure) {
        SharedClient.sharedInstance.start(request: AreaRequests.area(areRequestPayload: requestBody as! AreaRequestPlayload), model:AreaResponse(), success: { data in
            success(data)
        }, failure: { error in
            failure(error)
        })
    }
}
