//
//  AreaRequests.swift
//  Webook
//
//  Created by Apple on 28/01/21.
//

import Foundation
import  Alamofire
enum AreaRequests: Request {
    case area(areRequestPayload:AreaRequestPlayload)
    var path: String {
        return APIEndPoints.area.endpoint
    }
    var method: HTTPMethod {
        switch self {
        case .area:
            return .post
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case let .area(areRequestPayload):
            return areRequestPayload.parameters
        }
    }
    var headers: [String: String]? {
        switch self {
        case .area:
            let dictionary = ["Accept": "application/json"]
            return dictionary
        }
    }
    var cachePolicy: URLRequest.CachePolicy {
        return .reloadIgnoringCacheData
    }
    
    var dataType: DataType {
        return DataType.JSON
    }
}
