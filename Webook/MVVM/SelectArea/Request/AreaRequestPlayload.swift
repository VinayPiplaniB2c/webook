//
//  AreaRequestPlayload.swift
//  Webook
//
//  Created by Apple on 28/01/21.
//

import Foundation
import ObjectMapper

class AreaRequestPlayload: WBRequest {
    
    var cityid: String?
    
    init(cityid: String) {
        super.init()
        
        parameters?["cityid"] = cityid
    }
    
    required init?(map: Map) {
        super.init(map: map)
        
        cityid <- map["cityid"]
        
    }

}
