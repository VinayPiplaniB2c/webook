//
//  SignInViewModel.swift
//  UpConnectMe
//
//  Created by Pradeep Kumar on 30/01/2020.
//  Copyright © 2020 UpConnectMe. All rights reserved.
//

import Foundation

class LoginViewModel: HKBaseViewModel {
    
    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: (() -> ())?
    var didGetData: (() -> ())?
    var fotgotPasswordDidGetData: (() -> ())?
    var validationFailure: (() -> ())?
    var validationSuccess: (() -> ())?
    
    var authToken:String = ""
    private weak var loginViewController: SigninViewController?
    private weak var forgotViewController:ForgotPasswordVC?
    
    //MARK: -- Network checking
    /// Define networkStatus for check network connection
    var networkStatus = Reach().connectionStatus()
    
    /// Define boolean for internet status, call when network disconnected
    var isDisconnected: Bool = false {
        didSet {
            self.alertMessage = "No network connection. Please connect to the internet"
            self.internetConnectionStatus?()
        }
    }
    
    //MARK: -- UI Status
    
    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    override init(viewController: HKBaseViewController) {
        
        super.init(viewController: viewController)
        loginViewController = viewController as? SigninViewController
        forgotViewController = viewController as? ForgotPasswordVC
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
    }
    
    //MARK: Internet monitor status
    @objc func networkStatusChanged(_ notification: Notification) {
        self.networkStatus = Reach().connectionStatus()
    }
    
    //MARK: -- SignInAPI
    func SignInAPI(email: String?, password: String?) {
        switch networkStatus {
        case .offline:
            self.isDisconnected = true
            self.internetConnectionStatus?()
        case .online:
            loginAPI(email: email, password: password)
        default:
            break
        }
    }
    
    //MARK: -- forgotPassword
    func forgotPassword(email: String?) {
        switch networkStatus {
        case .offline:
            self.isDisconnected = true
            self.internetConnectionStatus?()
        case .online:
            forgotPasswordAPI(email: email)
        default:
            break
        }
    }
    
    //MARK: -- Verify Phone With FireBase Auth
//    func verifyPhone(mobileNumber: String?, password: String?) {
//        switch networkStatus {
//        case .offline:
//            self.isDisconnected = true
//            self.internetConnectionStatus?()
//        case .online:
//            verifyPhoneWithFireBaseAuth(mobileNumber: mobileNumber, password: password)
//        default:
//            break
//        }
//    }
}

extension LoginViewModel {
    
    private func loginAPI(email: String?, password: String?) {
        
        self.loginViewController?.showLoadingView()
        
        let loginRequest = LoginRequestPayload(email: email ?? "", password: password ?? "")
        let loginService = LoginService()
        loginService.requestBody = loginRequest
        loginService.login(success: { [weak self] response in
            
            self?.loginViewController?.hideLoadingView()
            if let responseModel = response as? LoginResponse {
                
                if responseModel.status == 0 {
                    print("\(responseModel.msg ?? "nil")")
                    self?.loginViewController?.showAlert(withTitle: "Error", andMessage: responseModel.msg ?? "Some error occure. Please try again.")
                    return
                }
                                
                if let isOtpVerified = responseModel.isOtpVerified {
                    if isOtpVerified == 1 {
                        
                        UserDefaults.standard.set(responseModel.data?.userId, forKey: "userId")
                        UserDefaults.standard.set(responseModel.data?.phone, forKey: "mobileNo")
                        UserDefaults.standard.set(responseModel.data?.name, forKey: "name")
                        UserDefaults.standard.set(responseModel.data?.tokenType, forKey: "tokenType")
                        UserDefaults.standard.setValue(responseModel.status == 1, forKey:keys.token)
                        UserDefaults.standard.synchronize()
                        AppDelegate.shared?.loginData = responseModel
                        
                        self?.didGetData?()
                        self?.loginViewController?.goToHomeTabBarController()
                    }else if isOtpVerified == 0 {
                        self?.validationFailure?()
                        self?.loginViewController?.showAlert(withTitle: "Error", andMessage: responseModel.msg ?? "Some error occure. Please try again.")
                    }
                }
            }
            
        }) { [weak self] (error) in
            self?.loginViewController?.hideLoadingView()
            if let error = error as? HKErrorCase{
                self?.handleGenericErrors(error: error)
            }
            print(error.debugDescription)
        }
    }
    
    private func forgotPasswordAPI(email: String?) {

        self.forgotViewController?.showLoadingView()

        let forgotPAsswordRequest = ForgotPasswordPayload(email:email ?? "")
        let loginService = LoginService()
        loginService.requestBody = forgotPAsswordRequest
        loginService.forgotPassword(success: { [weak self] response in

            self?.forgotViewController?.hideLoadingView()

            if let responseModel = response as? ForgotPasswordResponse {

                if let status = responseModel.status {
                    if status == 0 {
                        self?.forgotViewController?.showAlert(withTitle: "Message", andMessage: responseModel.msg ?? "Some error occure. Please try again")
                    }else{
//                        self?.forgotViewController?.showAlert(withTitle: "Message", andMessage: responseModel.msg ?? "Some error occure. Please try again")
                        self?.fotgotPasswordDidGetData?()


                    }
                }


            }

        }) { [weak self] (error) in
            self?.forgotViewController?.hideLoadingView()
            if let error = error as? HKErrorCase{
                self?.handleGenericErrors(error: error)
            }
            print(error.debugDescription)
        }
    }
    
//    private func verifyPhoneWithFireBaseAuth(mobileNumber: String?, password: String?) {
//
//        getBaseRequestModel(fullName: "", email: "", mobileNumber: mobileNumber, password: password, gender: "")
//        self.didGetData?()
//        return
//    }
    
   
    func getBaseRequestModel(name: String?, email: String?, phone: String?, password: String?) {
            
            if let phone = phone {
                if let password = password {
                    if let name = name {
                        if let email = email {
                            
                            if AppDelegate.shared?.baseRequestModel == nil {
                                AppDelegate.shared?.baseRequestModel = HKBaseRequestModel(name : name, email : email, phone: phone , password : password)
                            }
                        }
                    }
                }
            }
        }
}

