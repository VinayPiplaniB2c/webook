//
//  SigninViewController.swift
//  Webook
//
//  Created by BALA SEKHAR on 13/01/21.
//

import UIKit
import SkyFloatingLabelTextField

class SigninViewController: HKBaseViewController {
    
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextFieldWithIcon!
    
    var loginViewModel: LoginViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginViewModel = LoginViewModel(viewController: self)
        
        loginViewModel.didGetData = {[weak self] in
            print("loginViewModel from login")
            self?.loginViewModel?.viewController?.isFromSignUp = false
            AppDelegate.shared?.isFromSignUp = false
            self?.goToHomeTabBarController()
        }
//        loginViewModel.validationFailure = {[weak self] in
//
//            let mobileNumber = self?.emailTextField.text ?? ""
//            let password = self?.passwordTextField.text ?? ""
//            //            self?.loginViewModel?.verifyPhone(mobileNumber: mobileNumber, password: password)
//        }
        
        emailTextField.leftViewMode = .always
        passwordTextField.leftViewMode = .always
    }
    override func viewDidAppear(_ animated: Bool) {
        setStatusBar(backgroundColor: UIColor(red: 255/255, green:243/255, blue: 0/255, alpha: 1.0))
        
    }
    
    private func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }
    @IBAction func signupAction(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
    }
    
    @IBAction func signinAction(_ sender: UIButton) {
        if emailTextField.text == "" || passwordTextField.text == "" {
            showAlert(withTitle: "Message", andMessage: "Fields can not be empty")

        }else if !StringManager.sharedInstance.validateMobileLength(text:emailTextField.text!) {
            showAlert(withTitle: "Message", andMessage: "Please enter valid mobile number or email address ")
        }else if !StringManager.sharedInstance.validatePassword(text: passwordTextField.text!) {
            showAlert(withTitle: "Message", andMessage: "Please enter your valid password")
        }else{
            
            let mobileNumber = emailTextField.text ?? ""
            let password = passwordTextField.text ?? ""
            loginViewModel.SignInAPI(email: mobileNumber, password: password)
           
        }
        
       
        
    }
    
    
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        
    }
    
}
