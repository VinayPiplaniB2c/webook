//
//  LoginService.swift
//  HumanityKuotient
//
//  Created by Satya on 11/09/20.
//

import Foundation

class LoginService {
    var requestBody = WBRequest()
    
    func login(success: @escaping Success, failure: @escaping Failure) {
        SharedClient.sharedInstance.start(request: LoginRequests.login(loginRequestPayload: requestBody as! LoginRequestPayload), model: LoginResponse(), success: { data in
            success(data)
        }, failure: { error in
            failure(error)
        })
    }
    
    func forgotPassword(success: @escaping Success, failure: @escaping Failure) {
        SharedClient.sharedInstance.start(request: LoginRequests.forgot(forgotPasswordPayload: requestBody as! ForgotPasswordPayload), model: ForgotPasswordResponse(), success: { data in
            success(data)
        }, failure: { error in
            failure(error)
        })
    }
}
