//
//  LoginResponse.swift
//  HumanityKuotient
//
//  Created by Satya on 11/09/20.
//

import Foundation
import ObjectMapper

struct LoginResponse :  Mappable{
   
    var status: Int?
    var msg: String?
    var isOtpVerified: Int?
    var data : LoginData?
   

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        status <- map["status"]
        msg <- map["message"]
        isOtpVerified <- map["isOtpVerified"]  
        data <- map["data"]
        
    }
}

struct LoginData : Mappable{
    
    var userId : Int?
    var name : String?
    var address : String?
    var phone : String?
    var email : String?
    var tokenType : String?
    var token:String?
    
    

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        userId <- map["user_id"]
        name <- map["name"]
        address <- map["address"]
        phone <- map["mobile_no"]
        email <- map["email"]
        tokenType <- map["token_type"]
        token <- map["token"]
        
    }
}


struct ForgotPasswordResponse :  Mappable{

    var status: Int?
    var msg: String?
    var data : ForgotPasswordData?
   

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        data <- map["data"]
        msg <- map["message"]
        status <- map["status"]
    }
}

struct ForgotPasswordData : Mappable{

        var id: Int?
        var name, email: String?
        var gender, profilePic, countryCode: NSNull?
        var phone: String?
        var isOtpVerified: Int?
        var mobileVerifiedAt, address, city, state: NSNull?
        var country, pincode: NSNull?
        var otp, referCode: String?
        var amount: NSNull?
        var totalAmount: String?
        var status: Int?
        var socialtype, socialkey, token: NSNull?
        var isdeleted: Int?
        var deletedAt: NSNull?
        var updatedAt, createdAt: String?
    
    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
        referCode <- map["refer_code"]
        
    }
}

/**
 [2020-10-30 16:00:09.795] APIClient.swift:82 DEBUG: Optional({
     data =     {
         id = 67;
         isOtpVerified = 1;
     };
     msg = "OTP send to your register mobile";
     status = 1;
 })
 */
