//
//  LoginRequests.swift
//  HumanityKuotient
//
//  Created by Satya on 11/09/20.
//

import Foundation
import Alamofire

enum LoginRequests: Request {
    case login(loginRequestPayload: LoginRequestPayload)
    case forgot(forgotPasswordPayload: ForgotPasswordPayload)
    
    var path: String {
        switch self {
        case .login:
            let path = APIEndPoints.login.endpoint
            return path
        case .forgot:
            let path = APIEndPoints.forgot.endpoint
            return path
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .login, .forgot :
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case let .login(loginRequestPayload):
            return loginRequestPayload.parameters
        case let .forgot(forgotPasswordPayload):
            return forgotPasswordPayload.parameters
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .login, .forgot :
            let dictionary = ["Accept": "application/json"]
                //dictionary["Authorization"] = "Bearer \("token dsddsf")"
            return dictionary
        }
    }
    
    var cachePolicy: URLRequest.CachePolicy {
        return .reloadIgnoringCacheData
    }
    
    var dataType: DataType {
        return DataType.JSON
    }
}
