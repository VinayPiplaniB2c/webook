//
//  LoginRequestPayload.swift
//  HumanityKuotient
//
//  Created by Satya on 11/09/20.
//

import Foundation
import ObjectMapper

class LoginRequestPayload: WBRequest {
    
    var email: String?
    var password: String?
    
    init(email: String, password: String) {
        super.init()
        
        parameters?["email"] = email
        parameters?["password"] = password
        
    }
    
    required init?(map: Map) {
        super.init(map: map)
        
        email <- map["email"]
        password <- map["password"]
    }
}

class ForgotPasswordPayload: WBRequest {
    
    var email: String?
    
    init(email: String) {
        super.init()
        parameters?["email"] = email
    }
    
    required init?(map: Map) {
        super.init(map: map)
         email <- map["email"]
    }
}
