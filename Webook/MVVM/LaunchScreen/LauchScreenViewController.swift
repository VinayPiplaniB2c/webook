//
//  LauchScreenViewController.swift
//  Webook
//
//  Created by Apple on 06/02/21.
//

import UIKit

class LauchScreenViewController: HKBaseViewController {
    
    
    private let logoImageView:UIImageView = {
        let imageView=UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 150))
        imageView.contentMode = .scaleAspectFit
        imageView.image=UIImage(named: "LaunchScreenLogo")
        return imageView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(logoImageView)
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        logoImageView.center = view.center
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            self.animate()
        }
    }
    private func animate(){
        UIView.animate(withDuration: 1, animations:{
            let size=self.view.frame.size.width*1
            let diffX=size-self.view.frame.size.width
            let diffY=self.view.frame.size.height - size
            
            self.logoImageView.frame=CGRect(x: -(diffX/2), y: diffY/2, width: size, height: size)
        })
        
        UIView.animate(withDuration: 1.8, animations: {
            self.logoImageView.alpha = 1
        }, completion: { done in
            if done{
                DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                    let onboardingScreen = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
                    onboardingScreen.modalPresentationStyle = .fullScreen
                    self.present(onboardingScreen, animated: true)
                }
                
            }
            
        })
    }
    
  
}
