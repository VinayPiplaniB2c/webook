//
//  VerifyOTPViewModel.swift
//  HumanityKuotient
//
//  Created by Satya on 11/10/20.
//

import Foundation

class VerifyOTPViewModel: HKBaseViewModel {
    
    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: (() -> ())?
    var didGetData: (() -> ())?
    var otpVerified: (() -> ())?
    var phoneVerifyFailure: ((Error) -> ())?
    var phoneVerifySuccess: ((Bool?) -> ())?

    
    var authToken:String = ""
    private weak var verifyOtpViewController: VerifyOTPViewController?
    
    //MARK: -- Network checking
    /// Define networkStatus for check network connection
    var networkStatus = Reach().connectionStatus()
    
    /// Define boolean for internet status, call when network disconnected
    var isDisconnected: Bool = false {
        didSet {
            self.alertMessage = "No network connection. Please connect to the internet"
            self.internetConnectionStatus?()
        }
    }
    
    //MARK: -- UI Status
    
    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    override init(viewController: HKBaseViewController) {
        
        super.init(viewController: viewController)
        verifyOtpViewController = viewController as? VerifyOTPViewController
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
    }
    
    //MARK: Internet monitor status
    @objc func networkStatusChanged(_ notification: Notification) {
        self.networkStatus = Reach().connectionStatus()
    }
    
    //MARK: -- Verify Phone With FireBase Auth
//    func verifyPhone(mobileNumber: String?) {
//        switch networkStatus {
//        case .offline:
//            self.isDisconnected = true
//            self.internetConnectionStatus?()
//        case .online:
//            verifyPhoneWithFireBaseAuth(mobileNumber: mobileNumber)
//        default:
//            break
//        }
//    }
    
    //MARK: -- Verify Otp
//    func verifyOtp(otpCode: String?) {
//        switch networkStatus {
//        case .offline:
//            self.isDisconnected = true
//            self.internetConnectionStatus?()
//        case .online:
//            verifyOtpWithFireBaseAuth(otpCode: otpCode)
//        default:
//            break
//        }
//    }
    
    //MARK: -- login With API
    func loginWithAPI() {
        switch networkStatus {
        case .offline:
            self.isDisconnected = true
            self.internetConnectionStatus?()
        case .online:
            loginAPI()
        default:
            break
        }
    }
    
    //MARK: -- signup Step2 With API
    func signupAPIStep2(otp: String?, userId: String?) {
        switch networkStatus {
        case .offline:
            self.isDisconnected = true
            self.internetConnectionStatus?()
        case .online:
            signUpAPIStep2(otp: otp, userId: userId)
        default:
            break
        }
    }
}

extension VerifyOTPViewModel {
    
    private func signUpAPIStep2(otp: String?, userId: String?) {
        
//        self.verifyOtpViewController?.showLoadingView()
                
        let signupStep2Request = VerifyOtpRequestPayload(id: userId ?? "",
                                                         otp: otp ?? "")
        let signupService = SignUpService()
        signupService.requestBody = signupStep2Request
        
        signupService.signupStep2(success: { [weak self] response in
            
            if let responseModel = response as? VerifyOTPResponseModel {
                print("rsponse : \(responseModel)")
                if responseModel.status == 0 && responseModel.data == nil {
                    self?.verifyOtpViewController?.showAlert(withTitle: "Error", andMessage: responseModel.msg ?? "Some error occure. Please try again.")
                    return
                }else{

//                    self?.verifyOtpViewController?.showAlert(withTitle: "Error", andMessage: responseModel.msg ?? "Some error occure. Please try again.")

                }
                UserDefaults.standard.setValue(responseModel.status == 1, forKey:keys.token)


                self?.otpVerified?()

            }
//            self?.phoneVerifySuccess?(true)
//            self?.verifyOtpViewController?.hideLoadingView()
//            self?.verifyOtpViewController?.goToHomeTabBarController()


            
        }) { [weak self] (error) in
            self?.verifyOtpViewController?.hideLoadingView()
            if let error = error as? HKErrorCase{
                self?.handleGenericErrors(error: error)
            }
        }
    }
//    private func verifyPhoneWithFireBaseAuth(phone: String?) {
//
//        if let baseRequestModel = AppDelegate.shared?.baseRequestModel {
//
//            if let mobileNumber = baseRequestModel.phone {
//                self.verifyOtpViewController?.showLoadingView()
//                FirebaseAuth.shared.verifyPhoneNumber(phoneNumber: mobileNumber) {[weak self] (verificationId, error) in
//                    self?.verifyOtpViewController?.hideLoadingView()
//
//                    if error != nil{
//                        if let error = error {
//                            self?.phoneVerifyFailure?(error)
//                            return
//                        }
//                    }
//
//                    if verificationId != nil && error == nil {
//                        UserDefaults.standard.set(verificationId, forKey: "verificationId")
//                        self?.phoneVerifySuccess?(true)
//                    }
//                }
//            }
//        }
//    }
    
//    private func verifyOtpWithFireBaseAuth(otpCode: String?) {
//
//        if let otpCode = otpCode {
//            self.verifyOtpViewController?.showLoadingView()
//            FirebaseAuth.shared.verifyOtp(verificationCode: otpCode) { [weak self] (authDataResult, error) in
//                self?.verifyOtpViewController?.hideLoadingView()
//                if error != nil{
//                    if let error = error {
//                        self?.phoneVerifyFailure?(error)
//                        return
//                    }
//                }
//
//                if authDataResult?.user.phoneNumber != nil {
//                    self?.didGetData?()
//                }
//            }
//        }
//    }
    
    private func loginAPI() {
                
        if let baseRequestModel = AppDelegate.shared?.baseRequestModel {
            self.verifyOtpViewController?.showLoadingView()
            let loginRequest = LoginRequestPayload(email: baseRequestModel.email ?? "", password: baseRequestModel.password ?? "")
                let loginService = LoginService()
                loginService.requestBody = loginRequest
                loginService.login(success: { [weak self] response in
                    
                    self?.verifyOtpViewController?.hideLoadingView()
                    if let response = response as? LoginResponse {
                        
                        if response.status == 0 {
                            self?.verifyOtpViewController?.showAlert(withTitle: "Error", andMessage: response.msg ?? "Some error occure. Please try again.")
                            return
                        }
                        
                        UserDefaults.standard.set(response.data?.userId, forKey: "userId")
                        if let isOtpVerified = response.isOtpVerified {
                            if isOtpVerified == 1 {
                                 self?.verifyOtpViewController?.goToHomeTabBarController()
                            }
                        }
                    }
                    
                }) { [weak self] (error) in
                    self?.verifyOtpViewController?.hideLoadingView()
                    if let error = error as? HKErrorCase{
                        self?.handleGenericErrors(error: error)
                    }
                }
            }
        }
    
 

}
