//
//  VerifyOtpPayload.swift
//  HumanityKuotient
//
//  Created by Satya on 07/10/20.
//

import Foundation
import ObjectMapper

class VerifyOtpRequestPayload: WBRequest {
    
    var id: String?
    var otp: String?
    
    init(id: String, otp: String) {
        super.init()
        
        parameters?["id"] = id
        parameters?["otp"] = otp
        
    }
    
    required init?(map: Map) {
        super.init(map: map)
        
        id <- map["id"]
        otp <- map["otp"]
    }
}
