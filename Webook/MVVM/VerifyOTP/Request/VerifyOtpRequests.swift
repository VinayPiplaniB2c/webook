//
//  VerifyOtpRequests.swift
//  HumanityKuotient
//
//  Created by Satya on 07/10/20.
//

import Foundation
import Alamofire

enum VerifyOtpRequests: Request {
    case verifyotp(verifyOTPRequestPayload: VerifyOtpRequestPayload)
    
    var path: String {
        switch self {
        case .verifyotp:
            let path = APIEndPoints.verifyotp.endpoint
            return path
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .verifyotp :
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case let .verifyotp(verifyOTPRequestPayload):
            return verifyOTPRequestPayload.parameters
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .verifyotp :
            let dictionary = ["Accept": "application/json",
                              "Content-Type":"application/json"
                                ]
                //dictionary["Authorization"] = "Bearer \("token dsddsf")"
            return dictionary
        }
    }
    
    var cachePolicy: URLRequest.CachePolicy {
        return .reloadIgnoringCacheData
    }
    
    var dataType: DataType {
        return DataType.JSON
    }
}
