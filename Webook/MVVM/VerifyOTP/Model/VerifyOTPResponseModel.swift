//
//  VerifyOTPResponseModel.swift
//  Webook
//
//  Created by BALA SEKHAR on 22/01/21.
//

import Foundation
import ObjectMapper

struct VerifyOTPResponseModel :  Mappable {
   
    var msg: String?
    var status: Int?
    var data : [Array<Any>]?


    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        msg <- map["message"]
        status <- map["status"]
        data <- map["data"]
        
    }
}

