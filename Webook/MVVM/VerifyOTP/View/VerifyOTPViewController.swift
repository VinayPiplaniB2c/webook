//
//  VerifyOTPViewController.swift
//  Webook
//
//  Created by BALA SEKHAR on 15/01/21.
//

import UIKit

class VerifyOTPViewController: HKBaseViewController {
    
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var resendButtonOutlet: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
   
    private weak var verifyOTPViewController: VerifyOTPViewController?

    var sourceScreenName: String? = ""
    var otpCode: String? = nil
    var verifyOTPViewModel: VerifyOTPViewModel!
    
    let otpStackView = OTPStackView()
    var countdownTimer: Timer!
    var totalTime = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVerifyViewModel()
        otpView.addSubview(otpStackView)
        hideKeyboardWhenTappedAround()
        //        otpStackView.delegate = self
        otpStackView.heightAnchor.constraint(equalTo: otpView.heightAnchor).isActive = true
        otpStackView.centerXAnchor.constraint(equalTo: otpView.centerXAnchor).isActive = true
        otpStackView.centerYAnchor.constraint(equalTo: otpView.centerYAnchor).isActive = true
    }
    func setUpVerifyViewModel() {
        verifyOTPViewModel = VerifyOTPViewModel(viewController: self)

        verifyOTPViewModel.phoneVerifyFailure = { [weak self] error in
            self?.verifyOTPViewModel.viewController?.showAlert(withTitle: "Message", andMessage: error.localizedDescription)
        }
            
        verifyOTPViewModel.otpVerified = {[weak self] in
            print("verifyOTPViewModel from verify otp")
            
            if self?.sourceScreenName == "forgetPasswordVC" {
                self?.verifyOTPViewModel.viewController?.goToChangePasswordFromForgot()
            }else{
//                self?.goToHomeTabBarController()
                self?.goToSelectCityScreen()
            }
        }
        
    }
    
    
    
    
    
    @IBAction func verifyAction(_ sender: UIButton) {
        if let userId = UserDefaults.standard.value(forKey: "userId") {
            self.verifyOTPViewModel?.signupAPIStep2(otp:otpStackView.getOTP() , userId: "\(userId)")
        }
        print("Final OTP : ",otpStackView.getOTP())
        
//                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
//                let vc = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
//                vc.modalPresentationStyle = .fullScreen
//                present(vc, animated: true)
    }
    
    
    @IBAction func resendAction(_ sender: UIButton) {
        startTimer()
        resendButtonOutlet.setTitleColor(.lightGray, for: .normal)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
//            self.otpSentMessage.isHidden=true
        }
    }
    
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        timerLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
        resendButtonOutlet.setTitleColor(.yellow, for: .normal)
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
}
extension VerifyOTPViewController: OTPDelegate {
    
    func didChangeValidity(isValid: Bool) {
        //        testButton.isHidden = !isValid
    }
    
}
