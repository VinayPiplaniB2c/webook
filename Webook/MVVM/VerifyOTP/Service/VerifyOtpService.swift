//
//  VerifyOtpService.swift
//  HumanityKuotient
//
//  Created by Satya on 07/10/20.
//

import Foundation

class VerifyOtpService {
    var requestBody = WBRequest()
    
    func verifyOtp(success: @escaping Success, failure: @escaping Failure) {
        SharedClient.sharedInstance.start(request: LoginRequests.login(loginRequestPayload: requestBody as! LoginRequestPayload), model: LoginResponse(), success: { data in
            success(data)
        }, failure: { error in
            failure(error)
        })
    }
}
