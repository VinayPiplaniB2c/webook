

//  AppConstants.swift
//  Webook
//
//  Created by BALA SEKHAR on 21/01/21.
//
import Foundation

enum AppConstants {
    // MARK: - Regular expression constant
    enum Regex {
        static let ifscCode = "^[A-Za-z]{4}0[A-Z0-9a-z]{6}$"
        static let date = "yyyy-MM-dd"
        static let dateAndTime = "yyyy-MM-dd HH:mm:ss"
        static let time = "hh:mm a"
        static let timeIn24hr = "HH:mm"
        static let password = "^(?=.{8,})(?=.*[\\u0600-\\u065F\\u066A-\\u06EF\\u06FA-\\u06FFa-zA-Z])(?=.*[\\u0660-\\u06690-9])(?=.*[-.!@#$%_\"’\\^\\&\\?])[^\\s]*$"
        static let dateWithMonthName = "dd MMM yyyy"
        static let dateWithMonthNameNotificationList = "dd MMMM yyyy"
        
        static let weekday = "EEEE"
        static let MMddyyyy = "dd-MM-yyyy"
        static let firstName = "^[\\u0600-\\u065F\\u066A-\\u06EF\\u06FA-\\u06FFa-zA-Z .’-]*$"
        static let middleLastName = "^[\\u0600-\\u065F\\u066A-\\u06EF\\u06FA-\\u06FFa-zA-Z .’-]*$"
        static let email = "^([a-zA-Z0-9\\+_\\-]+)(\\.[a-zA-Z0-9\\+_\\-]+)*@([a-z0-9\\-]+\\.)+[a-z]{2,6}$"
        static let isdCode = "^[0-9]{1,4}$"
        static let mobileNumber = "^[0-9]{8,12}$"
        static let securityAnswer = "^[\\u0600-\\u065F\\u066A-\\u06EF\\u06FA-\\u06FFa-zA-Z0-9 -!@#$%_\"’\\^\\&*.\\?]*$"
        static let contactUSDesc = "^[\\u0600-\\u065F\\u066A-\\u06EF\\u06FA-\\u06FFa-zA-Z0-9 -!,\\[\\]@()#$%_\"’\\^\\&*.\\?]{1,200}"
        static let numberOfEntries = "^[0-9]*$"
        static let favSeriesName = "^[\\u0600-\\u065F\\u066A-\\u06EF\\u06FA-\\u06FFa-zA-Z .’-]*$"
        static let favSeriesNumber = "^[0-9]{1,2}$"
        static let streetAddress = "^[\\u0600-\\u065F\\u066A-\\u06EF\\u06FA-\\u06FF\\u00C0-\\u01E0a-zA-Z0-9 .,’#\\/-]*$"
        static let stateOrCity = "^[\\u0600-\\u065F\\u066A-\\u06EF\\u06FA-\\u06FF\\u00C0-\\u01E0 a-zA-Z .’,-]*$"
        static let postalCode = "^[a-zA-Z0-9]{0,15}$"
        static let fullName = "^[a-zA-Z .'-]*$"
        static let residentialAddress = "^[a-zA-Z0-9 .,’#\\/-]*$"
        static let bank_branch_city = "^[\\u00C0-\\u01E0 a-zA-Z .’,-]*$"
        static let identificationNumber = "^[a-zA-Z0-9]{0,25}$"
    }
    
    // MARK: - Request headers
    enum RequestHeaders {
        static let contentType = "Content-Type"
        static let applicationJSON = "application/json"
        static let formData = "application/x-www-form-urlencoded"
        //static let authorization = "Authorization"
        //static let bearer = "Bearer "
        //static let apiAccessKey = "api-access-key"
        //static let apiAccessValue = "b1b1d7942d833d1bd94fa321418430f2"
    }
}
