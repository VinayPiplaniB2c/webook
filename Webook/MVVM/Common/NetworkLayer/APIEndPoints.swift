//
//  APIEndPoints.swift
//  HumanityKuotient
//
//  Created by Satya on 11/09/20.
//

import Foundation

enum APIEndPoints: String {

    case login
    case forgot
    case signup
    case verifyotp
    case city
    case area
    case changePassword
    case dashboard
    case listing
   
    case loginWithfacebook
    case loginWithGoogle
    case privacy
    case about
    case faq
    case ProfileData
    case updateProfile
    case resend
    case sendRequest
    
   
    

    var endpoint: String {
        switch self {
        case .login:
            return "login"
        case .forgot:
            return "forgot_password"
        case .signup:
            return "signup"
        case .verifyotp:
            return "verify_otp"
        case .city:
            return "city"
        case .area:
            return "area"
        case .changePassword:
            return "reset_password"
        case .dashboard:
            return "dashboard"
        case .listing:
            return "listing"
       
        case .loginWithfacebook:
            return "auth/facebook"
        case .loginWithGoogle:
            return "auth/google"
        case .privacy:
            return "privacy"
        case .about:
            return "about"
        
        case .faq:
            return "faq"
        case .ProfileData:
            return "ProfileData"
        case .updateProfile:
            return "updateProfile"
        case .resend:
            return "resend"
        
        case .sendRequest:
            return "sendRequest"
            
        
        }
    }
}
