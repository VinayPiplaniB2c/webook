
//  HKRequest.swift
//  Webook
//
//  Created by BALA SEKHAR on 21/01/21.
//
import Alamofire
import Foundation

public enum DataType {
    case JSON
    case data
}

public protocol Request {
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: [String: Any]? { get }
    var headers: [String: String]? { get }
    var cachePolicy: URLRequest.CachePolicy { get }
    var dataType: DataType { get }
}

import ObjectMapper

class WBRequest: Mappable {
    var parameters: [String: Any]?
    
    init() {
        parameters = [:]
    }
    
    required init?(map: Map) {
        parameters = [:]
    }
    
    func mapping(map: Map) {
        
    }
}

struct MultipartAPIRequest {
    let url: String!
    let params: [String: String]
    var httpHeaders: [String: String]
    let data: Data?
    let imgName: String!

    init(url: String!, params: [String: String]!, httpHeaders: [String: String], contentType: String? = nil, data: Data?, imgName: String) {
        self.url = url
        self.params = params
        self.httpHeaders = httpHeaders
        self.data = data
        self.imgName = imgName
        
        if contentType == nil {
            self.httpHeaders[AppConstants.RequestHeaders.contentType] = AppConstants.RequestHeaders.formData
        }
    }

    static func addRequestHeader(key: String, value: String) -> [String: String] {
        var requestHeader = [String: String]()
        requestHeader[key] = value
        return requestHeader
    }
}
