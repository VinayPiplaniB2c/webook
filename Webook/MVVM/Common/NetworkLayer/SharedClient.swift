
//  SharedClient.swift
//  Webook
//
//  Created by BALA SEKHAR on 21/01/21.
//

import Foundation

final class SharedClient: APIClient {
    
    static let sharedInstance = SharedClient()
    
    let baseUrl:String = "http://b2cdomain.in/serenity/api/"

    private init() {}
    
    var errorModel: Convertable {
        return HKError()
    }
    
    var validStatusCodes: [Int] {
        return Array(200 ..< 300)
    }
    
    var headers: [String: String]? {
                
        return ["Content-Type": "application/json"]
        //, "Authorization": "Bearer \(token ?? "")"
    }
    
    func getBaseUrl() -> String {
        return baseUrl
    }
}
