//
//  ChangePasswordVC.swift
//  Webook
//
//  Created by BALA SEKHAR on 15/01/21.Openeye_Icon
//Closeeye_Icon

import UIKit
import SkyFloatingLabelTextField
class ChangePasswordVC:HKBaseViewController  {
    
    @IBOutlet weak var newPasswordTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var confirmPasswordTextField: SkyFloatingLabelTextFieldWithIcon!
    
    
    var changePasswordViewModel: ChangePasswordViewModel!

    let eyeButtonOne = UIButton(type: .custom)
    let eyeButtonTwo = UIButton(type: .custom)
    
    var eyeButtonOneBool:Bool = false
    var eyeButtonTwoBool:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eyeButtonSetup()
        changePasswordCallBack()
        
    }
    func changePasswordCallBack(){
        changePasswordViewModel = ChangePasswordViewModel(viewController: self)
        
        changePasswordViewModel.changePasswordDidGetData = {[weak self] in
            print("ChangepasswordViewModel from forgotpassword")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self?.goToHomeTabBarController()

            }
        }
    }
    func eyeButtonSetup(){
        //eyeone
        eyeButtonOne.setImage(UIImage(named: "Closeeye_Icon"), for: .normal)
        eyeButtonOne.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        eyeButtonOne.frame = CGRect(x: CGFloat(newPasswordTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        eyeButtonOne.addTarget(self, action: #selector(eyeOneButton), for: .touchUpInside)
        newPasswordTextField.rightView = eyeButtonOne
        newPasswordTextField.rightViewMode = .always
        
        //eyetwo
        eyeButtonTwo.setImage(UIImage(named: "Closeeye_Icon"), for: .normal)
        eyeButtonTwo.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        eyeButtonTwo.frame = CGRect(x: CGFloat(confirmPasswordTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        eyeButtonTwo.addTarget(self, action: #selector(eyeTwoButton), for: .touchUpInside)
        confirmPasswordTextField.rightView = eyeButtonTwo
        confirmPasswordTextField.rightViewMode = .always
    }
    override func viewDidAppear(_ animated: Bool) {
        setStatusBar(backgroundColor: UIColor(red: 255/255, green:243/255, blue: 0/255, alpha: 1.0))
        
    }
    
    private func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }
    
    @objc func eyeOneButton(_ sender: Any) {
        
        if eyeButtonOneBool {
            eyeButtonOneBool = false
            eyeButtonOne.setImage(UIImage(named:"Closeeye_Icon"), for: UIControl.State.normal)
            newPasswordTextField.isSecureTextEntry=true
        } else {
            eyeButtonOneBool = true
            eyeButtonOne.setImage(UIImage(named:"Openeye_Icon"), for: UIControl.State.normal)
            newPasswordTextField.isSecureTextEntry=false
        }
        
    }
    @objc func eyeTwoButton(_ sender: Any) {
        if eyeButtonTwoBool {
            eyeButtonTwoBool = false
            eyeButtonTwo.setImage(UIImage(named:"Closeeye_Icon"), for: UIControl.State.normal)
            confirmPasswordTextField.isSecureTextEntry=true
        } else {
            eyeButtonTwoBool = true
            eyeButtonTwo.setImage(UIImage(named:"Openeye_Icon"), for: UIControl.State.normal)
            confirmPasswordTextField.isSecureTextEntry=false
            
        }
        
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        if newPasswordTextField.text == "" || confirmPasswordTextField.text == "" {
            showAlert(withTitle: "Message", andMessage: "Fields can not be empty")

        }else if !StringManager.sharedInstance.validatePassword(text: newPasswordTextField.text!) {
            showAlert(withTitle: "Message", andMessage: "Please use minimum 6 or more characters to change password")
        }else if !StringManager.sharedInstance.validatePassword(text: confirmPasswordTextField.text!) {
            showAlert(withTitle: "Message", andMessage: "Please use minimum 6 or more characters to change password")
        }else{
            let password = newPasswordTextField.text ?? ""
            
            if let confirmPassword = confirmPasswordTextField.text {
                if password != confirmPassword {
                    showAlert(withTitle: "Message", andMessage: "Confirm password should be match with password")
                    return
                }
            }
            let userId=UserDefaults.standard.value(forKey: "userId") ?? ""
//            let otp=UserDefaults.standard.value(forKey: "")
            print("Sending user id \(userId) to change password")
            
            changePasswordViewModel.changePassword(id: "\(userId)", otp: "1234", password: password)
        }

    }
    
    
}
