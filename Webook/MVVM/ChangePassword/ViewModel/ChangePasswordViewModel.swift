//
//  ChangePasswordViewModel.swift
//  Webook
//
//  Created by Apple on 27/01/21.
//

import Foundation
class ChangePasswordViewModel: HKBaseViewModel {
    
    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: (() -> ())?
    var changePasswordDidGetData: (() -> ())?
    var validationFailure: (() -> ())?
    var validationSuccess: (() -> ())?
    
    private weak var changePasswordViewController: ChangePasswordVC?
    
    //MARK: -- Network checking
    /// Define networkStatus for check network connection
    var networkStatus = Reach().connectionStatus()
    
    /// Define boolean for internet status, call when network disconnected
    var isDisconnected: Bool = false {
        didSet {
            self.alertMessage = "No network connection. Please connect to the internet"
            self.internetConnectionStatus?()
        }
    }
    
    //MARK: -- UI Status
    
    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    override init(viewController: HKBaseViewController) {
        
        super.init(viewController: viewController)
        changePasswordViewController = viewController as? ChangePasswordVC
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
    }
    
    //MARK: Internet monitor status
    @objc func networkStatusChanged(_ notification: Notification) {
        self.networkStatus = Reach().connectionStatus()
    }
    //MARK: -- ChangePassword
    func changePassword(id: String?,otp:String?,password:String?) {
        switch networkStatus {
        case .offline:
            self.isDisconnected = true
            self.internetConnectionStatus?()
        case .online:
            changePasswordAPI(id: id, otp: otp, password: password)
        default:
            break
        }
    }
    
    
}

extension ChangePasswordViewModel{
    private func changePasswordAPI(id: String?,otp:String?,password:String?) {
        
        self.changePasswordViewController?.showLoadingView()
        
        let changePasswordRequest = ChangePasswordRequestPayload(id: id ?? "", otp: otp ?? "", password: password ?? "")
        let changePasswordService = ChangePasswordService()
        changePasswordService.requestBody = changePasswordRequest
        changePasswordService.changePassword(success: { [weak self] response in
            
            self?.changePasswordViewController?.hideLoadingView()
            
            if let responseModel = response as? ChangePasswordResponse {
                
                if let status = responseModel.status {
                    if status == 0 {
                        self?.changePasswordViewController?.showAlert(title: "Message", message: responseModel.msg ?? "Some error occure. Please try again")
                    }
                    self?.changePasswordDidGetData?()
                    
                }
            }
            
        }) { [weak self] (error) in
            self?.changePasswordViewController?.hideLoadingView()
            if let error = error as? HKErrorCase{
                self?.handleGenericErrors(error: error)
            }
            print(error.debugDescription)
        }
    }
    
}
