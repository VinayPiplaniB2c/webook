//
//  ChangePasswordService.swift
//  Webook
//
//  Created by Apple on 27/01/21.
//

import Foundation

class ChangePasswordService {
    var requestBody = WBRequest()
    
    func changePassword(success: @escaping Success, failure: @escaping Failure) {
        SharedClient.sharedInstance.start(request:ChangePasswordRequest.changePassword(changePasswordRequestPayload: requestBody as! ChangePasswordRequestPayload), model: ChangePasswordResponse(), success: { data in
            success(data)
        }, failure: { error in
            failure(error)
        })
    }
    
}
