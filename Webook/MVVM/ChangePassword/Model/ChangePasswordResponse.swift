//
//  ChangePasswordResponse.swift
//  Webook
//
//  Created by Apple on 27/01/21.
//

import Foundation
import ObjectMapper

struct ChangePasswordResponse :  Mappable{
   
    var msg: String?
    var status: Int?
    var data : ChangePasswordData?
   

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        status <- map["status"]
        msg <- map["message"]
        data <- map["data"]
        
    }
}

struct ChangePasswordData : Mappable {
    
    var userId : Int?
    var tokenType, token: String?
    
    

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        userId <- map["user_id"]
        tokenType <- map["token_type"]
        token <- map["token"]
        
    }
}
