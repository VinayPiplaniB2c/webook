//
//  ChangePasswordRequest.swift
//  Webook
//
//  Created by Apple on 27/01/21.
//

import Foundation
import Alamofire

enum ChangePasswordRequest: Request {
    case changePassword(changePasswordRequestPayload: ChangePasswordRequestPayload)
    
    var path: String {
        switch self {
        case .changePassword:
            let path = APIEndPoints.changePassword.endpoint
            return path
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .changePassword:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
        case let .changePassword(changePasswordRequestPayload):
            return changePasswordRequestPayload.parameters
        }
    }
    
    var headers: [String: String]? {
        switch self {
        case .changePassword :
            let dictionary = ["Accept": "application/json"]
                //dictionary["Authorization"] = "Bearer \("token dsddsf")"
            return dictionary
        }
    }
    
    var cachePolicy: URLRequest.CachePolicy {
        return .reloadIgnoringCacheData
    }
    
    var dataType: DataType {
        return DataType.JSON
    }
}
