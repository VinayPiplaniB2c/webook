//
//  ChangePasswordRequestPayload.swift
//  Webook
//
//  Created by Apple on 27/01/21.
//

import Foundation
import ObjectMapper

class ChangePasswordRequestPayload: WBRequest {
    
    var id: String?
    var otp: String?
    var password:String?
    
    init(id: String, otp: String,password:String) {
        super.init()
        
        parameters?["id"] = id
        parameters?["otp"] = otp
        parameters?["password"] = password
        
    }
    
    required init?(map: Map) {
        super.init(map: map)
        
        id <- map["id"]
        otp <- map["otp"]
        password <- map["password"]
    }

}
