//
//  UserNameTableViewCell.swift
//  Webook
//
//  Created by Apple on 05/02/21.
//

import UIKit

class UserNameTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
