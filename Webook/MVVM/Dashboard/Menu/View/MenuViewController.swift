//
//  MenuViewController.swift
//  Webook
//
//  Created by Apple on 05/02/21.
//

import UIKit
class MenuViewController: UIViewController {

    @IBOutlet weak var menuTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       tableViewSetup()
    }
    private func tableViewSetup(){
        menuTableView.delegate = self
        menuTableView.dataSource = self
        
        menuTableView.register(UINib(nibName: "UserNameTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "UserNameTableViewCellId")
        menuTableView.register(UINib(nibName: "MenuCustomTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "MenuCustomTableViewCellId")
    }


}
extension MenuViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return ""
        }else if section == 1{
            return "Search by Library"
        }else if section == 2{
            return ""
        }
        return ""
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else if section == 1{
            return 33
        }else if section == 2{
            return 1
        }
        return 0.00
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else {return}
        header.textLabel?.textColor = .darkGray
        header.textLabel?.font = UIFont(name: "CircularAirPro-Book", size: 16)
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .left
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
           if indexPath.row == 0{
                return 100
           }else{
           }
        }else{
            return 50.00
        }
        return 50.00
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else if section == 1{
            return 2
        }else if section == 2{
            return 6
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        //section=0
        if indexPath.section == 0{
            if indexPath.row == 0{
                let userNameCell:UserNameTableViewCell=menuTableView.dequeueReusableCell(withIdentifier: "UserNameTableViewCellId", for: indexPath) as! UserNameTableViewCell
                userNameCell.selectionStyle = .none
                let userName=UserDefaults.standard.string(forKey: "name")
                userNameCell.userNameLabel.text="\(userName ?? "User")"
                return userNameCell
            }else{
                
            }
            
        //section=1
        }else if indexPath.section == 1{
            if indexPath.row == 0{
                let librariesCell:MenuCustomTableViewCell=menuTableView.dequeueReusableCell(withIdentifier: "MenuCustomTableViewCellId", for: indexPath) as! MenuCustomTableViewCell
                librariesCell.selectionStyle = .none
                librariesCell.tableViewLabel.text="Libraries"
                return librariesCell
            }else if indexPath.row == 1{
                let locationsCell:MenuCustomTableViewCell=menuTableView.dequeueReusableCell(withIdentifier: "MenuCustomTableViewCellId", for: indexPath) as! MenuCustomTableViewCell
                locationsCell.selectionStyle = .none

                locationsCell.tableViewLabel.text="Locations"
                return locationsCell
            }
        //section=2
        }else if indexPath.section == 2{
            if indexPath.row == 0{
                let myBookkingsCell:MenuCustomTableViewCell=menuTableView.dequeueReusableCell(withIdentifier: "MenuCustomTableViewCellId", for: indexPath) as! MenuCustomTableViewCell
                myBookkingsCell.selectionStyle = .none
                myBookkingsCell.tableViewLabel.text="My Bookings"
                return myBookkingsCell
            }else if indexPath.row == 1{
                let wishlistCell:MenuCustomTableViewCell=menuTableView.dequeueReusableCell(withIdentifier: "MenuCustomTableViewCellId", for: indexPath) as! MenuCustomTableViewCell
                wishlistCell.selectionStyle = .none
                wishlistCell.tableViewLabel.text="Wishlists"
                return wishlistCell
            }else if indexPath.row == 2{
                let referAndEarnCell:MenuCustomTableViewCell=menuTableView.dequeueReusableCell(withIdentifier: "MenuCustomTableViewCellId", for: indexPath) as! MenuCustomTableViewCell
                referAndEarnCell.selectionStyle = .none
                referAndEarnCell.tableViewLabel.text="Refer and Earn"
                return referAndEarnCell
            }else if indexPath.row == 3{
                let aboutCell:MenuCustomTableViewCell=menuTableView.dequeueReusableCell(withIdentifier: "MenuCustomTableViewCellId", for: indexPath) as! MenuCustomTableViewCell
                aboutCell.selectionStyle = .none
                aboutCell.tableViewLabel.text="About"
                return aboutCell
            }else if indexPath.row == 4{
                let suggestionOrFeedbackCell:MenuCustomTableViewCell=menuTableView.dequeueReusableCell(withIdentifier: "MenuCustomTableViewCellId", for: indexPath) as! MenuCustomTableViewCell
                suggestionOrFeedbackCell.selectionStyle = .none
                suggestionOrFeedbackCell.tableViewLabel.text="Suggestion/Feedback"
                return suggestionOrFeedbackCell
            }else if indexPath.row == 5{
                let helpAndSupportCell:MenuCustomTableViewCell=menuTableView.dequeueReusableCell(withIdentifier: "MenuCustomTableViewCellId", for: indexPath) as! MenuCustomTableViewCell
                helpAndSupportCell.selectionStyle = .none
                helpAndSupportCell.tableViewLabel.text="Help & Support"
                return helpAndSupportCell
            }
        }
        return cell
    }
    //DidSelectRowAt
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
            } else if indexPath.row == 1 {
                
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                
//                performSegue(withIdentifier: "goToLibrariesListFromMenu", sender: self)
            } else if indexPath.row == 1 {
                
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
            }else if indexPath.row == 1 {
            }else if indexPath.row == 2{
            }else if indexPath.row == 3{
                performSegue(withIdentifier: "goToAboutScreenFromMenu", sender: self)
            }else if indexPath.row == 4{
                performSegue(withIdentifier: "goToSuggestionAndFeedbackFromMenu", sender: self)
            }else if indexPath.row == 5{
                performSegue(withIdentifier: "goToHelpScreenFromMenu", sender: self)
            }
        }
    }
    
}
