//
//  HomeViewModel.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import Foundation
class HomeViewModel: HKBaseViewModel {
    
    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: ((HKErrorCase) -> ())?
    var didGetData: ((HomeResponse) -> ())?
    
    var authToken:String = ""
    private weak var homeViewController: HomeViewController?
    
    //MARK: -- Network checking
    /// Define networkStatus for check network connection
    var networkStatus = Reach().connectionStatus()
    
    /// Define boolean for internet status, call when network disconnected
    var isDisconnected: Bool = false {
        didSet {
            self.alertMessage = "No network connection. Please connect to the internet"
            self.internetConnectionStatus?()
        }
    }
    
    //MARK: -- UI Status
    
    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    override init(viewController: HKBaseViewController) {
        
        super.init(viewController: viewController)
        homeViewController = viewController as? HomeViewController
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
    }
    
    //MARK: Internet monitor status
    @objc func networkStatusChanged(_ notification: Notification) {
        self.networkStatus = Reach().connectionStatus()
    }
    
    //MARK: -- Verify Phone With FireBase Auth
    func homeAPI() {
        switch networkStatus {
        case .offline:
            self.isDisconnected = true
            self.internetConnectionStatus?()
        case .online:
            homeAPICall()
        default:
            break
        }
    }
}
extension HomeViewModel {
    private func homeAPICall() {
        
        self.homeViewController?.showLoadingView()
        
        let homeRequestPayload = HomeRequestPlayload()
        let homeService = HomeServices()
        homeService.requestBody = homeRequestPayload
        homeService.home(success: {[weak self] (response) in
            
            self?.homeViewController?.hideLoadingView()
            
            if let responseModel = response as? HomeResponse {
                print("homeresponseModel--->\(responseModel)")
                self?.didGetData?(responseModel)
            }
            
        }) { [weak self] (error) in
            self?.homeViewController?.hideLoadingView()
            if let error = error as? HKErrorCase {
                self?.serverErrorStatus?(error)
            }
        }
    }
}
