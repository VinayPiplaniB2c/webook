//
//  HomeServices.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import Foundation
class HomeServices {
    var requestBody = WBRequest()
    func home(success: @escaping Success, failure: @escaping Failure) {
        SharedClient.sharedInstance.start(request: HomeRequests.home(homeRequestPayload: requestBody as! HomeRequestPlayload), model:HomeResponse(), success: { data in
            success(data)
        }, failure: { error in
            failure(error)
        })
    }
}
