//
//  HomeRequests.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import Foundation
import  Alamofire
enum HomeRequests: Request {
    case home(homeRequestPayload:HomeRequestPlayload)
    var path: String {
        return APIEndPoints.dashboard.endpoint
    }
    var method: HTTPMethod {
        switch self {
        case .home:
            return .post
        }
    }
    var parameters: [String: Any]? {
        return [:]
    }
    var headers: [String: String]? {
        switch self {
        case .home:
            let dictionary = ["Accept": "application/json",
                              "Content-Type":"application/json"]
            return dictionary
        }
    }
    var cachePolicy: URLRequest.CachePolicy {
        return .reloadIgnoringCacheData
    }
    
    var dataType: DataType {
        return DataType.JSON
    }
}
