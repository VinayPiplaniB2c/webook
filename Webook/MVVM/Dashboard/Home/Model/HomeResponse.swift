//
//  HomeResponse.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import Foundation
import ObjectMapper

struct HomeResponse :  Mappable{
   
    var status: Int?
    var msg: String?
    var data: HomeData?
    var code: Int?
    
   
    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        status <- map["status"]
        msg <- map["message"]
        data <- map["data"]
        
    }
}

struct HomeData : Mappable{
    
    var sliders: [Slider]?
    var libraries: [Library]?
    var brands: [Brand]?
    var totalStudent, totalLibraries: Int?
   

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        sliders <- map["Sliders"]
        libraries <- map["libraries"]
        brands <- map["brands"]
        totalStudent <- map["total_student"]
        totalLibraries <- map["total_libraries"]
        
    }
}
struct Slider : Mappable{
    var id: Int?
    var shortText, sliderDescription: String?
    var image: String?
    

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        id <- map["id"]
        shortText <- map["short_text"]
        sliderDescription <- map["description"]
        image <- map["image"]
        
    }
}
struct Library : Mappable{
        var id: Int?
        var name, city, area: String?
        var image: String?
        var rating: String?
    

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        city <- map["city"]
        area <- map["area"]
        image <- map["image"]
        rating <- map["rating"]

        
    }
}
struct Brand : Mappable{
    var image: String?
    var name: String?
    var id: Int?
    

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        image <- map["image"]
        name <- map["name"]
        id <- map["id"]

    }
}
