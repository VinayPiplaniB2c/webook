//
//  BrandListViewController.swift
//  Webook
//
//  Created by BALA SEKHAR on 18/01/21.
//

import UIKit

class BrandListViewController: UIViewController {
    
    @IBOutlet weak var brandListTableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableviewSetup()
    }
    override func viewDidAppear(_ animated: Bool) {
        setStatusBar(backgroundColor: UIColor(red: 255/255, green:243/255, blue: 0/255, alpha: 1.0))

    }
   
    private func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }
    private func tableviewSetup(){
        brandListTableview.delegate=self
        brandListTableview.dataSource=self
    }
    
}
extension BrandListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let brandCell:BrandListTableViewCell=brandListTableview.dequeueReusableCell(withIdentifier: "brandCell", for: indexPath) as! BrandListTableViewCell
        
        return brandCell
    }
    
    
}
