//
//  HomeViewController.swift
//  Webook
//
//  Created by BALA SEKHAR on 16/01/21.
//

import UIKit

class HomeViewController: HKBaseViewController {
    
    @IBOutlet weak var homeTableview: UITableView!
    @IBOutlet weak var librarySearch: UISearchBar!
    
    var homeViewModel: HomeViewModel!
    var homeResponseModel: HomeResponse?
    var howWeWorkData=[HowWeWork]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeViewModel = HomeViewModel(viewController: self)
        homeViewModel.homeAPI()
        setupViewModelCallBacks()
        tableviewSetup()
        customSearchbarSetup()
    }
    override func viewDidAppear(_ animated: Bool) {
        setStatusBar(backgroundColor: UIColor(red: 255/255, green:243/255, blue: 0/255, alpha: 1.0))

    }
   
    private func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }
    func customSearchbarSetup(){
        librarySearch.layer.cornerRadius=20
        librarySearch.layer.masksToBounds=true
        if let textfield = librarySearch.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = UIColor.white
        }
        
    }
    func setupViewModelCallBacks(){
        homeViewModel.serverErrorStatus = { [weak self] error in
            self?.showAlert(withTitle: "Message", andMessage: error.localizedDescription)
        }
        
        homeViewModel.didGetData = {[weak self] model in
            print("HomeViewModel \(model)")
            self?.setupUI(homeResponseModel: model)
            
        }
    }
    
    func setupUI(homeResponseModel: HomeResponse?) {
        if let homeResponseModel = homeResponseModel {
            self.homeResponseModel = homeResponseModel
            self.homeTableview.reloadData()
        }
    }
    private func tableviewSetup(){
        homeTableview.delegate=self
        homeTableview.dataSource=self
        //BannerTableViewCell
        homeTableview.register(UINib(nibName: "BannerTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "BannerTableViewCellId")
        //LibrariesTableViewCell
        homeTableview.register(UINib(nibName: "LibrariesTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "LibrariesTableViewCellId")
        //BrandsTableViewCell
        homeTableview.register(UINib(nibName: "BrandsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "BrandsTableViewCellId")
        //TotalStudentsTableViewCell
        homeTableview.register(UINib(nibName: "TotalStudentsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TotalStudentsTableViewCellId")
        //TotalLibrariesTableViewCell
        homeTableview.register(UINib(nibName: "TotalLibrariesTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "TotalLibrariesTableViewCellId")
        //HowweworkTableViewCell
        homeTableview.register(UINib(nibName: "HowweworkTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "HowweworkTableViewCellId")
        //CustomHeaderView
        homeTableview.register(UINib(nibName: "CustomHeaderView", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: "CustomHeaderView")
        
        
        homeTableview.rowHeight = UITableView.automaticDimension
    }
    
    @IBAction func menuAction(_ sender: UIButton) {
        
        
    }
    
    @IBAction func locationPinAction(_ sender: UIButton) {
        
        
    }
    
    
    
    
}

extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view=UIView()
        if section == 1{
            let librariesHeader:CustomHeaderView=homeTableview.dequeueReusableHeaderFooterView(withIdentifier: "CustomHeaderView") as! CustomHeaderView
            librariesHeader.contentView.backgroundColor = UIColor.white
            librariesHeader.headerTitleLabel.text = "Popular Libraries"
            librariesHeader.viewAllIBOutlet.addTarget(self, action: #selector(gotoPopularLibraries), for: .touchUpInside)
            return librariesHeader
        }else if section == 2{
            let brandsHeader:CustomHeaderView=homeTableview.dequeueReusableHeaderFooterView(withIdentifier: "CustomHeaderView") as! CustomHeaderView
            brandsHeader.contentView.backgroundColor = UIColor.white

            brandsHeader.headerTitleLabel.text = "Brands"
            brandsHeader.viewAllIBOutlet.addTarget(self, action: #selector(gotoBrands), for: .touchUpInside)
            return brandsHeader
            
        }else if section == 5{
            let howWeWorkHeader = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
            
            let label = UILabel()
            label.frame = CGRect.init(x: 5, y: 5, width: howWeWorkHeader.frame.width-10, height: howWeWorkHeader.frame.height-10)
            label.text = "How we work"
            label.font = UIFont(name: "CircularAirPro-Bold", size: 24)
            label.textColor = UIColor.black
            
            howWeWorkHeader.addSubview(label)
            return howWeWorkHeader
        }
        return view
    }
    @objc private func gotoPopularLibraries(){
        print("gotoPopularLibraries")
        self.goToPopularLibraries()
    }
    @objc private func gotoBrands(){
        print("gotoBrands")
        self.goToBrandsList()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = indexPath.section
        
        if section == 0 {
            return 200.00
        } else if section == 1 {
            return 250.00
        } else if section == 2 {
            return 200.00
        }else if section == 3 {
            return 180.00
        }else if section == 4 {
            return 180.00
        }else if section == 5 {
            return 200.00
        }
        return 250.00
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else if section == 2 {
            return 1
        }else if section == 3 {
            return 1
        }else if section == 4 {
            return 1
        }else if section == 5 {
            return 1
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        let section = indexPath.section
        //BannerTableViewCell
        if section == 0{
            let bannerCell:BannerTableViewCell=homeTableview.dequeueReusableCell(withIdentifier: "BannerTableViewCellId", for: indexPath) as! BannerTableViewCell
            let bannerCellData = homeResponseModel?.data?.sliders
            bannerCell.setup(slidersData: bannerCellData)
            
            
            return bannerCell
            //LibrariesTableViewCell
        }else if section == 1{
            let librariesCell:LibrariesTableViewCell=homeTableview.dequeueReusableCell(withIdentifier: "LibrariesTableViewCellId", for: indexPath) as! LibrariesTableViewCell
            let librariesCellData = homeResponseModel?.data?.libraries
            librariesCell.setup(libraryData: librariesCellData)
            
            return librariesCell
            //BrandsTableViewCell
        }else if section == 2 {
            let brandsCell:BrandsTableViewCell=homeTableview.dequeueReusableCell(withIdentifier: "BrandsTableViewCellId", for: indexPath) as! BrandsTableViewCell
            let brandsCellData = homeResponseModel?.data?.brands
            brandsCell.setup(brandData: brandsCellData)
            
            
            return brandsCell
            //TotalStudentsTableViewCell
        }else if section == 3 {
            let totalStudentsCell:TotalStudentsTableViewCell=homeTableview.dequeueReusableCell(withIdentifier: "TotalStudentsTableViewCellId", for: indexPath) as! TotalStudentsTableViewCell
            
            totalStudentsCell.totalStudents.text="\(homeResponseModel?.data?.totalStudent ?? 0)"
            
            return totalStudentsCell
            //TotalLibrariesTableViewCell
        }else if section == 4 {
            let totalLibrariesCell:TotalLibrariesTableViewCell=homeTableview.dequeueReusableCell(withIdentifier: "TotalLibrariesTableViewCellId", for: indexPath) as! TotalLibrariesTableViewCell
            
            totalLibrariesCell.totalLibraries.text = "\(homeResponseModel?.data?.totalLibraries ?? 0)"
            
            return totalLibrariesCell
            //HowweworkTableViewCell
        }else if section == 5 {

             let howweworkCell:HowweworkTableViewCell=homeTableview.dequeueReusableCell(withIdentifier: "HowweworkTableViewCellId", for: indexPath) as! HowweworkTableViewCell

            return howweworkCell
        }
        return cell
    }
    
    
}
