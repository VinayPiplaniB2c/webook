//
//  PopularLibraryService.swift
//  Webook
//
//  Created by Apple on 11/02/21.
//

import Foundation
class PopularLibraryService {
    var requestBody = WBRequest()
    func popularLibrary(success: @escaping Success, failure: @escaping Failure) {
        SharedClient.sharedInstance.start(request: PopularLibraryRequests.popularLibraries(popularLibraryRequestPayload: requestBody as! PopularLibraryRequestPlayload), model:PopularLibraryListResponse(), success: { data in
            success(data)
        }, failure: { error in
            failure(error)
        })
    }
}



