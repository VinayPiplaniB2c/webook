//
//  PopularLibraryRequestPlayload.swift
//  Webook
//
//  Created by Apple on 11/02/21.
//

import Foundation
import ObjectMapper

class PopularLibraryRequestPlayload: WBRequest {
    
    var city: String?
    var area: String?
    
    init(city: String, area: String) {
        super.init()
        
        parameters?["city"] = city
        parameters?["area"] = area
        
    }
    
    required init?(map: Map) {
        super.init(map: map)
        
        city <- map["city"]
        area <- map["area"]
    }
}
