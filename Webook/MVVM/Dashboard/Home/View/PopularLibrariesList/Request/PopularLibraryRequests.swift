//
//  PopularLibraryRequests.swift
//  Webook
//
//  Created by Apple on 11/02/21.
//

import Foundation
import  Alamofire
enum PopularLibraryRequests: Request {
    case popularLibraries(popularLibraryRequestPayload:PopularLibraryRequestPlayload)
    var path: String {
        return APIEndPoints.listing.endpoint
    }
    var method: HTTPMethod {
        switch self {
        case .popularLibraries:
            return .post
        }
    }
    var parameters: [String: Any]? {
        switch self {
        case let .popularLibraries(popularLibraryRequestPayload):
            return popularLibraryRequestPayload.parameters
        }
    }
    var headers: [String: String]? {
        switch self {
        case .popularLibraries:
            let dictionary = ["Accept": "application/json",
                              "Content-Type":"application/json"]
            return dictionary
        }
    }
    var cachePolicy: URLRequest.CachePolicy {
        return .reloadIgnoringCacheData
    }
    
    var dataType: DataType {
        return DataType.JSON
    }
}
