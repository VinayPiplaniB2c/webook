//
//  AreaCollectionViewCell.swift
//  Webook
//
//  Created by Apple on 10/02/21.
//

import UIKit

class AreaCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var areaName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellBackgroundView.backgroundColor = .white

        cellBackgroundView.layer.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor
        cellBackgroundView.layer.cornerRadius = 8
        cellBackgroundView.layer.borderWidth = 1
        cellBackgroundView.layer.borderColor = UIColor(red: 1, green: 0.953, blue: 0, alpha: 1).cgColor
    }
    func librarySetupData(model:PopularLibraryData) {
        areaName.text=model.area
    }
}
