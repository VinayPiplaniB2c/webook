//
//  AreaCollectionTableViewCell.swift
//  Webook
//
//  Created by Apple on 10/02/21.
//

import UIKit

class AreaCollectionTableViewCell: UITableViewCell {

    @IBOutlet weak var areaListCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        areaCollectionSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    private func areaCollectionSetup(){
        areaListCollectionView.delegate=self
        areaListCollectionView.dataSource=self
        areaListCollectionView.register(UINib(nibName: "AreaCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AreaCollectionViewCellId")
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = CGSize(width: 140, height: 50)
        layout.scrollDirection = .horizontal
        areaListCollectionView.collectionViewLayout = layout
    }
    
}
extension AreaCollectionTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let areaCollectionCell:AreaCollectionViewCell=areaListCollectionView.dequeueReusableCell(withReuseIdentifier: "AreaCollectionViewCellId", for: indexPath) as! AreaCollectionViewCell
        
        return areaCollectionCell
    }
    
    
}
