//
//  PopularLibraryViewModel.swift
//  Webook
//
//  Created by Apple on 11/02/21.
//

import Foundation
class PopularLibraryViewModel: HKBaseViewModel {
    var internetConnectionStatus: (() -> ())?
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var serverErrorStatus: ((HKErrorCase) -> ())?
    var popularLibraryDidGetData: ((PopularLibraryListResponse) -> ())?
    var validationFailure: ((String) -> ())?
    var validationSuccess: (() -> ())?
    
    private weak var popularLibraryListViewController:  PopularLibrariesListViewController?

    //MARK: -- Network checking
    /// Define networkStatus for check network connection
    var networkStatus = Reach().connectionStatus()
    
    /// Define boolean for internet status, call when network disconnected
    var isDisconnected: Bool = false {
        didSet {
            self.alertMessage = "No network connection. Please connect to the internet"
            self.internetConnectionStatus?()
        }
    }
    
    //MARK: -- UI Status
    
    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    override init(viewController: HKBaseViewController) {
        
        super.init(viewController: viewController)
        popularLibraryListViewController = viewController as? PopularLibrariesListViewController
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        
    }
    
    //MARK: Internet monitor status
    @objc func networkStatusChanged(_ notification: Notification) {
        self.networkStatus = Reach().connectionStatus()
    }
    //MARK: -- CityAPI
    func PopularLibraryAPI(city:String?,area:String?) {
        switch networkStatus {
        case .offline:
            self.isDisconnected = true
            self.internetConnectionStatus?()
        case .online:
            popularLibrayApi(city:city,area:area)
        default:
            break
        }
    }
}
extension PopularLibraryViewModel {
    
    private func popularLibrayApi(city:String?,area:String?) {
        self.popularLibraryListViewController?.showLoadingView()
       
        let popularLibraryRequest = PopularLibraryRequestPlayload(city: city ?? "", area: area ?? "")
        let popularLibraryService = PopularLibraryService()
        popularLibraryService.requestBody = popularLibraryRequest
        popularLibraryService.popularLibrary(success: { [weak self] response in
            self?.popularLibraryListViewController?.hideLoadingView()
            if let responseModel = response as? PopularLibraryListResponse {
                print("popularLibraryresponseModel:--->\(responseModel)")
                self?.popularLibraryDidGetData?(responseModel)
            }
        }) { [weak self] (error) in
            self?.popularLibraryListViewController?.hideLoadingView()
            if let error = error as? HKErrorCase{
                self?.handleGenericErrors(error: error)
            }
            print(error.debugDescription)
        }
    }
}
