//
//  PopularLibraryListResponse.swift
//  Webook
//
//  Created by Apple on 11/02/21.
//

import Foundation
import ObjectMapper

struct PopularLibraryListResponse :  Mappable{
   
        var status: Bool?
        var message: String?
        var data: [PopularLibraryData]?
    
   
    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
        
    }
}

struct PopularLibraryData : Mappable{
    
        var id: Int?
        var name, city, area, address: String?
        var image: String?
        var seats: Int?
        var price, mrpPrice, favourite, rating: String?
   

    init?(map: Map){}
    init(){}

    mutating func mapping(map: Map)
    {
        id <- map["id"]
        name <- map["name"]
        city <- map["city"]
        area <- map["area"]
        address <- map["address"]
        
        image <- map["image"]
        seats <- map["seats"]
        price <- map["price"]
        mrpPrice <- map["mrp_price"]
        favourite <- map["favourite"]
        rating <- map["rating"]

        
    }
}
