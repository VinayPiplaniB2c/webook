//
//  PopularLibrariesListViewController.swift
//  Webook
//
//  Created by Apple on 10/02/21.
//

import UIKit

class PopularLibrariesListViewController: HKBaseViewController {
    
    @IBOutlet weak var popularLibrariesListTable: UITableView!
    
    var popularLibraryViewModel: PopularLibraryViewModel!
    var popularLibraryResponseModel: PopularLibraryListResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PopularLibrariesListTableViewSetup()
        
        popularLibraryViewModel = PopularLibraryViewModel(viewController: self)
        setupViewModelCallBacks()
    }
    override func viewDidAppear(_ animated: Bool) {
        setStatusBar(backgroundColor: UIColor(red: 255/255, green:243/255, blue: 0/255, alpha: 1.0))
    }
    private func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }
    private func PopularLibrariesListTableViewSetup(){
        popularLibrariesListTable.delegate=self
        popularLibrariesListTable.dataSource=self
        
        popularLibrariesListTable.register(UINib(nibName: "AreaCollectionTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "AreaCollectionTableViewCellId")
        
        popularLibrariesListTable.register(UINib(nibName: "SimilarLibrariesTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "SimilarLibrariesTableViewCellId")
        
    }
    func setupViewModelCallBacks(){
        popularLibraryViewModel.serverErrorStatus = { [weak self] error in
            self?.showAlert(withTitle: "Message", andMessage: error.localizedDescription)
        }
        
        popularLibraryViewModel.popularLibraryDidGetData = {[weak self] model in
            print("PopularLibraryViewModel \(model)")
            self?.setupUI(popularLibarayResponseModel: model)
            
        }
    }
    func setupUI(popularLibarayResponseModel: PopularLibraryListResponse?) {
        if let popularLibraryListResponseModel = popularLibarayResponseModel {
            self.popularLibraryResponseModel = popularLibraryListResponseModel
            self.popularLibrariesListTable.reloadData()
        }
    }
    
}
extension PopularLibrariesListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 1
        case 1:
            return 10
        default:
            break
        }
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let section = indexPath.section
        
        switch section {
        case 0:
            return 50
        case 1:
            return 280
        default:
            break
        }
        return 280
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell=UITableViewCell()
        
        let section = indexPath.section
        switch section {
        case 0:
            let areCollections:AreaCollectionTableViewCell=popularLibrariesListTable.dequeueReusableCell(withIdentifier: "AreaCollectionTableViewCellId") as! AreaCollectionTableViewCell
            
            return areCollections
        case 1:
            let popularLibraries:SimilarLibrariesTableViewCell=popularLibrariesListTable.dequeueReusableCell(withIdentifier: "SimilarLibrariesTableViewCellId") as! SimilarLibrariesTableViewCell
            
            return popularLibraries
        default:
            break
        }
        
        
        return cell
    }
    
    
}
