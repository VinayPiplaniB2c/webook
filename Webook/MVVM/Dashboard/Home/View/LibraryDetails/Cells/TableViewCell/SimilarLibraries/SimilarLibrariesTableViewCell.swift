//
//  SimilarLibrariesTableViewCell.swift
//  Webook
//
//  Created by Apple on 10/02/21.
//

import UIKit
import IBAnimatable
import Kingfisher

class SimilarLibrariesTableViewCell: UITableViewCell {

    @IBOutlet weak var libraryImage: AnimatableImageView!
    @IBOutlet weak var wishlistButton: AnimatableButton!
    @IBOutlet weak var libraryName: UILabel!
    @IBOutlet weak var numberOfSeats: UILabel!
    @IBOutlet weak var libraryLocation: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var mrpPrice: UILabel!
    
    var wishListBool:Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func wishlistAction(_ sender: UIButton) {
        if wishListBool {
            wishListBool = false
            wishlistButton.setImage(UIImage(named:"WishlistNotSave_Icon"), for: UIControl.State.normal)
        } else {
            wishListBool = true
            wishlistButton.setImage(UIImage(named:"WishListSave_Icon"), for: UIControl.State.normal)
        }
    }
    
    func librarySetupData(model:PopularLibraryData) {
        //image
        guard let imageUrlStr = model.image else {
            return
        }
        let escapedString = imageUrlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let imageURL = URL(string: escapedString ?? "")
        DispatchQueue.main.async {
            self.libraryImage.kf.setImage(with: imageURL)
        }
        //name
        libraryName.text=model.name
        //
        numberOfSeats.text="\(model.seats ?? 0)"
        libraryLocation.text = "\(model.city ?? "city")" + ",\(model.area ?? "area")"
        price.text="Rs." + "\(model.price ?? "price")" + "/hr"
        mrpPrice.text="Rs." + "\(model.mrpPrice ?? "mrpprice")" + "/hr"
        
    }
    
    
}
