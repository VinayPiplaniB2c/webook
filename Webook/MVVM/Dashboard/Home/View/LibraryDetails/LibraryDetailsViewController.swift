//
//  LibraryDetailsViewController.swift
//  Webook
//
//  Created by Apple on 08/02/21.
//

import UIKit

class LibraryDetailsViewController: HKBaseViewController {
    
    
    @IBOutlet weak var libraryTableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        libraryTableviewSetup()
        
        
    }
    
    func libraryTableviewSetup(){
        libraryTableview.delegate=self
        libraryTableview.dataSource=self
        
        //LibraryDetailsTableViewCellOne
        libraryTableview.register(UINib(nibName: "LibraryDetailsTableViewCellOne", bundle: Bundle.main), forCellReuseIdentifier: "LibraryDetailsTableViewCellOneId")
        
        libraryTableview.rowHeight = UITableView.automaticDimension
        
        
    }
    
    
}
extension LibraryDetailsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let libraryCell:LibraryDetailsTableViewCellOne=libraryTableview.dequeueReusableCell(withIdentifier: "LibraryDetailsTableViewCellOneId") as! LibraryDetailsTableViewCellOne
        
        return libraryCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
}
