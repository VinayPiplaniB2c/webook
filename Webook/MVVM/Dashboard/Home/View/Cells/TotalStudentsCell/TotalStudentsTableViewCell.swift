//
//  TotalStudentsTableViewCell.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import UIKit

class TotalStudentsTableViewCell: UITableViewCell {

    @IBOutlet weak var totalStudents: UILabel!
    
    var totalStudentsData = Data()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setup(totalaStudent:Data?) {
        guard let totalaStudents = totalaStudent else { return  }
        totalStudentsData = totalaStudents
       
        
    }
    
}
