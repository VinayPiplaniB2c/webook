//
//  LibrariesTableViewCell.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import UIKit
import Kingfisher
class LibrariesTableViewCell: UITableViewCell {

    
    @IBOutlet weak var librariesCollectionView: UICollectionView!
    
    var librariesData = [Library]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        librariesCollectionViewSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    private func librariesCollectionViewSetup(){
        librariesCollectionView.delegate=self
        librariesCollectionView.dataSource=self
        
        librariesCollectionView.register(UINib(nibName: "LibrariesCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "LibrariesCollectionViewCellId")
        setCollectionViewLayouts()
    }
    func setup(libraryData:[Library]?) {
        guard let libraries = libraryData else { return  }
        librariesData = libraries
        librariesCollectionView.reloadData()
        
    }
    
    func setCollectionViewLayouts(){
        let flowLayout=UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        flowLayout.itemSize=CGSize(width: self.librariesCollectionView.frame.width/2-10, height: 250)
        flowLayout.minimumLineSpacing = 5
        flowLayout.minimumInteritemSpacing = 5
        
        self.librariesCollectionView.setCollectionViewLayout(flowLayout, animated: true)
        
    }
}
extension LibrariesTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return librariesData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let libraryCell:LibrariesCollectionViewCell=librariesCollectionView.dequeueReusableCell(withReuseIdentifier: "LibrariesCollectionViewCellId", for: indexPath) as! LibrariesCollectionViewCell
        let model = librariesData[indexPath.row]
        libraryCell.librarySetupData(model: model)
                
        return libraryCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAt-->\(indexPath.item)")
    }
    
    
}

