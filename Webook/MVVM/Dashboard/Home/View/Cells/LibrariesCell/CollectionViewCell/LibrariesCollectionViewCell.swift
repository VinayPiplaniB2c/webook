//
//  LibrariesCollectionViewCell.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import UIKit
import IBAnimatable
import Cosmos
import Kingfisher

class LibrariesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var librariesImage: UIImageView!
    @IBOutlet weak var areLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var libraryNameLabel: UILabel!
    @IBOutlet weak var ratingStarView: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ratingStarView.isUserInteractionEnabled = false
    }
    func librarySetupData(model:Library) {
        
        areLabel.text = model.area
        cityLabel.text = model.city
        libraryNameLabel.text = model.name
        ratingStarView.rating = Double("\(model.rating ?? "0.0")") ?? 0.0

//        let double = Double(model.rating ?? Int(0.0))
//        print("Library rating :-->\(double)")
//        ratingStarView.rating =  double
        
        
        guard let imageUrlStr = model.image else {
            return
        }
        let escapedString = imageUrlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

//        print("Library Images Str is \(escapedString ?? "")")
        let imageURL = URL(string: escapedString ?? "")
        DispatchQueue.main.async {
            self.librariesImage.kf.setImage(with: imageURL)
        }
        
        
        
    }
}
