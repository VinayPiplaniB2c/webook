//
//  BannerTableViewCell.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import UIKit

class BannerTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var bannerPageControll: UIPageControl!
    
    var homeResponseModel: HomeResponse?

    var sliderData:[Slider]=[]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bannerPageControll.numberOfPages=Int(homeResponseModel?.data?.sliders?.count ?? 2)
//        bannerPageControll.currentPage = 0
        bannerCollectionViewSetup()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    private func bannerCollectionViewSetup(){
        bannerCollectionView.delegate=self
        bannerCollectionView.dataSource=self
        
        bannerCollectionView.register(UINib(nibName: "BannerCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "BannerCollectionViewCellId")
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        bannerPageControll.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        
    }
    func setup(slidersData:[Slider]?) {
        guard let sliders = slidersData else { return  }
        sliderData = sliders
        bannerCollectionView.reloadData()
        
    }
 
    @IBAction func bannerPageChange(_ sender: Any) {
        let pc = sender as! UIPageControl
        
        // scrolling the collectionView to the selected page
        bannerCollectionView.scrollToItem(at: IndexPath(item: pc.currentPage, section: 0),
                                           at: .centeredHorizontally, animated: true)
    }
    
}
extension BannerTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliderData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bannerCell:BannerCollectionViewCell=bannerCollectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCellId", for: indexPath) as! BannerCollectionViewCell
        let model = sliderData[indexPath.row]
        bannerCell.setupData(model: model)
        
        return bannerCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAt-->\(indexPath.item)")
    }
    
    
}
extension BannerTableViewCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width:collectionView.frame.width, height: collectionView.frame.height)
    }
}
