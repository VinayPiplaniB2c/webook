//
//  BannerCollectionViewCell.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import UIKit
import Kingfisher

class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var bannerTitle: UILabel!
    @IBOutlet weak var bannerDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bannerTitle.isHidden = true
        bannerDescription.isHidden = true
    }
    func setupData(model:Slider) {
        
//        bannerTitle.text = model.shortText
//        bannerDescription.text = model.sliderDescription
        
        
        guard let imageUrlStr = model.image else {
            return
        }
        let escapedString = imageUrlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
//        print("Banner Images Str is \(escapedString ?? "")")
        let imageURL = URL(string: escapedString ?? "")
        DispatchQueue.main.async {
            self.bannerImage.kf.setImage(with: imageURL)
        }
        
        
        
    }
    

}
