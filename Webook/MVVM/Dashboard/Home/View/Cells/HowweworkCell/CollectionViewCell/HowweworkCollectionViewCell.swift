//
//  HowweworkCollectionViewCell.swift
//  Webook
//
//  Created by BALA SEKHAR on 18/01/21.
//

import UIKit

class HowweworkCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var howWeWorkBGImage: UIImageView!
    @IBOutlet weak var howWeWorkContentLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func howweworkConfigureCell(page: HowWeWork){

        // set the title and description of the screen
        self.howWeWorkBGImage.image = page.howweworkImage
        self.howWeWorkContentLabel.text = page.howweworkDescription

    }
}
