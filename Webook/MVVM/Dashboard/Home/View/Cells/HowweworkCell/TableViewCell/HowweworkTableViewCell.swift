//
//  HowweworkTableViewCell.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import UIKit
struct HowWeWork {
    let howweworkImage: UIImage
    let howweworkDescription: String
}

class HowweworkTableViewCell: UITableViewCell {

    @IBOutlet weak var howweworkCollectionview: UICollectionView!
   
    var howWeWorkData: [HowWeWork] = [HowWeWork(howweworkImage:#imageLiteral(resourceName: "SearchtheLibraryBG_Icon") , howweworkDescription: "Search theLibrary"),
                                      HowWeWork(howweworkImage: #imageLiteral(resourceName: "SelectYourSeatBG_Icon"), howweworkDescription: "Select your Seat"),
                                      HowWeWork(howweworkImage: #imageLiteral(resourceName: "StudySelectyourcomfortably_Icon"), howweworkDescription: "Study comfortably")
                                        ]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionViewSetup()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func collectionViewSetup(){
        howweworkCollectionview.delegate=self
        howweworkCollectionview.dataSource=self
        
        howweworkCollectionview.register(UINib(nibName: "HowweworkCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "HowweworkCollectionViewCellId")
        setCollectionViewLayouts()
    }
    func setCollectionViewLayouts(){
        let flowLayout=UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        flowLayout.itemSize=CGSize(width: self.howweworkCollectionview.frame.width/2-10, height: 150)
        flowLayout.minimumLineSpacing = 10
        flowLayout.minimumInteritemSpacing = 10
        
        howweworkCollectionview.setCollectionViewLayout(flowLayout, animated: true)
        
    }
    func setup(howweworkData:[HowWeWork]) {
        let howweworks = howweworkData
        howWeWorkData = howweworks
        howweworkCollectionview.reloadData()

    }
}
extension HowweworkTableViewCell:UICollectionViewDataSource,UICollectionViewDelegate{
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return howWeWorkData.count
//        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let howWeWorkCell:HowweworkCollectionViewCell=collectionView.dequeueReusableCell(withReuseIdentifier: "HowweworkCollectionViewCellId", for: indexPath) as! HowweworkCollectionViewCell
        let model = howWeWorkData[indexPath.row]
        howWeWorkCell.howweworkConfigureCell(page: model)
//        howWeWorkCell.howWeWorkBGImage.image=model.howweworkImage
//        howWeWorkCell.howWeWorkContentLabel.text=model.howweworkDescription

        return howWeWorkCell
    }
    
    
}
