//
//  BrandsCollectionViewCell.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import UIKit
import Kingfisher

class BrandsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var brandImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func brandData(model:Brand) {
       
        guard let imageUrlStr = model.image else {
            return
        }
        let escapedString = imageUrlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

//        print("Brands Images Str is \(escapedString ?? "")")
        
        let imageURL = URL(string: escapedString ?? "")
        DispatchQueue.main.async {
            self.brandImage.kf.setImage(with: imageURL)
        }
        
    }
}
