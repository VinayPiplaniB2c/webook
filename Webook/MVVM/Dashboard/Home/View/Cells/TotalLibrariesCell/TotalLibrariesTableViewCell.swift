//
//  TotalLibrariesTableViewCell.swift
//  Webook
//
//  Created by Apple on 29/01/21.
//

import UIKit

class TotalLibrariesTableViewCell: UITableViewCell {

    
    @IBOutlet weak var totalLibraries: UILabel!
    
    var totalLibrariesData = Data()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setup(totalLibraries:Data?) {
        guard let totalLibrary = totalLibraries else { return  }
        totalLibrariesData = totalLibrary
       
        
    }
}
