//
//  NotificationsViewController.swift
//  Webook
//
//  Created by BALA SEKHAR on 19/01/21.
//

import UIKit

class NotificationsViewController: UIViewController {

    
    @IBOutlet weak var notificationTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableviewSetup()
    }
    
    private func tableviewSetup(){
        notificationTableView.delegate=self
        notificationTableView.dataSource=self
    }
   

}
extension NotificationsViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notificationCell:NotificationTableViewCell=notificationTableView.dequeueReusableCell(withIdentifier: "notificationTableViewCellId", for:indexPath) as! NotificationTableViewCell
        
        return notificationCell
    }
    
    
}
