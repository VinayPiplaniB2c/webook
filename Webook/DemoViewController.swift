//
//  DemoViewController.swift
//  Webook
//
//  Created by Apple on 09/02/21.
//

import UIKit

class DemoViewController: UIViewController {

  
    @IBOutlet weak var demoTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        demoTableView.delegate=self
        demoTableView.dataSource=self
        
        
    }
    

    

}
extension DemoViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=demoTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }

}
